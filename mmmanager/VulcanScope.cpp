///////////////////////////////////////////////////////////////////////////////////////////////////
// FILE:          VulanScope.cpp
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   VulanScope adapters
//                                                                                     
// AUTHOR:        K.GireeshWaran, 22 Feb 2017
//                Based on LeicaDSMTC adapter by G. Esteban Fernandez
//
//COPYRIGHT:		  Spectral Inisights Pvt Ltd

#ifdef WIN32
#include <windows.h>
#endif

#ifdef linux
#include <unistd.h>
#endif

#include <cstdio>
#include <string>
#include <math.h>
#include <iterator>
#include <iostream>
#include <algorithm> 
#include "VulcanScopeHub.h"
#include "VulcanScope.h"

// Static Initialization
long int XYZCom::limitX_ = 0;
long int XYZCom::limitY_ = 0;
long int XYZCom::limitZ_ = 0;

long int XYZCom::microStepX_ = 4;
long int XYZCom::microStepY_ = 4;
long int XYZCom::microStepZ_ = 4;
long int XYZCom::microStepTurret_ = 4;
long int XYZCom::modeX_ = 0;
long int XYZCom::modeY_ = 0;
long int XYZCom::modeZ_ = 0;
long int XYZCom::modeT_ = 0;
long int XYZCom::XYRunCurrent_ = 10;
long int XYZCom::ZRunCurrent_ = 10;
long int XYZCom::TurretRunCurrent_ = 10;
long int XYZCom::XYHoldCurrent_ = 8;
long int XYZCom::ZHoldCurrent_ = 8;
long int XYZCom::TurretHoldCurrent_ = 8;
bool JoyStick::JoyStickEnabled_ = false;

// Device strings
const char* g_VulanScopeHub = "Scope";
const char* g_VulanScopeXYDrive = "XYStage";
const char* g_VulanScopeZDrive = "ZStage";
const char* g_VulanScopeXYZDrive = "XYZCom";
const char* g_VulanScopeLamp = "WhiteLamp";
const char* g_VulanScopeObjLens = "ObjectiveLens";
const char* g_VulanScopeJoyStick = "JoyStick";


// Action Name
// XYStage
const char* g_OnSpeedXY = "SpeedXY";
const char* g_OnRampXY = "RampXY";
const char* g_OnDirectionX = "DirectionX";
const char* g_OnDirectionY = "DirectionY";
const char* g_OnXTrigger = "XTrigger";
const char* g_OnYTrigger = "YTrigger";
const char* g_OnZTrigger = "ZTrigger";
const char* g_OnXYTriggerPostDelay = "XYTriggerPostDelay";
const char* g_OnXYTriggerPreDelay = "XYTriggerPreDelay";
const char* g_OnZTriggerPostDelay = "ZTriggerPostDelay";
const char* g_OnZTriggerPreDelay = "ZTriggerPreDelay";
const char* g_OnEnableTrigger = "EnableTrigger";
const char* g_OnEnableCommandLine = "EnableCommandLine";
const char* g_OnGetStepSizeXYUm = "GetStepSizeXYUm";
const char* g_OnEnableNewAcqTrigger = "EnableNewAcqTrigger";
const char* g_OnSetZStackTable = "SetZStackTable";
const char* g_OnSendDesiredCommand = "SendDesiredCommand";
const char* g_OnSetPulseDurationX = "SetPulseDurationX";
const char* g_OnSetPulseDurationY = "SetPulseDurationY";

// ZStage
const char* g_OnSpeedZ = "SpeedZ";
const char* g_OnDirectionZ = "DirectionZ";
const char* g_OnRampZ = "RampZ";
const char* g_OnBacklashZ = "BacklashZ";
const char* g_OnZStackYSteps = "ZStackYSteps";
const char* g_OnZStackXSteps = "ZStackXSteps";
const char* g_OnSetZStack = "SetZStack";
const char* g_OnZStackSetInstance = "ZStackSetInstance";
const char* g_OnXYTriggerPostDelayZ = "XYTriggerPostDelayZ";
const char* g_OnXYTriggerPreDelayZ = "XYTriggerPreDelayZ";
const char* g_OnZTriggerPostDelayZ = "ZTriggerPostDelayZ";
const char* g_OnZTriggerPreDelayZ = "ZTriggerPreDelayZ";
const char* g_OnSetZStackSize = "SetZStackSize";
const char* g_OnSetZStackStepSize = "SetZStackStepSize";
const char* g_OnSetZFgBgMap = "SetZFgBgMap";
const char* g_OnSetPulseDurationZ = "SetPulseDurationZ";

// XYZCom
const char* g_OnEnableXYZ = "EnableXYZ";
const char* g_OnSetLimitX = "SetLimitX";
const char* g_OnSetLimitY = "SetLimitY";
const char* g_OnSetLimitZ = "SetLimitZ";
const char* g_OnEnableSetLimitXYZ = "EnableSetLimitXYZ";
const char* g_OnSetMicroStepX = "MicroStepX";
const char* g_OnSetMicroStepY = "MicroStepY";
const char* g_OnSetMicroStepZ = "MicroStepZ";
const char* g_OnSetMicroStepTurret = "MicroStepTurret";
const char* g_OnEnableSetMicroStepXYZT = "EnableMicroStepXYZ";
const char* g_OnSetFLDFilter = "SetFilter";
const char* g_OnGetInfo = "GetInfo";

//XYZ Driver Setting
const char* g_XYRunCurrentSetting = "XYRunCurrent";
const char* g_ZRunCurrentSetting = "ZRunCurrent";
const char* g_TurretRunCurrentSetting = "TurretRunCurrent";
const char* g_XYHoldCurrentSetting = "XYHoldCurrent";
const char* g_ZHoldCurrentSetting = "ZHoldCurrent";
const char* g_TurretHoldCurrentSetting = "TurretHoldCurrent";
const char* g_OnEnableCurrentSettingXY = "EnableCurrentSettingXY";
const char* g_OnEnableCurrentSettingZ = "EnableCurrentSettingZ";
const char* g_OnEnableCurrentSettingTurret = "EnableCurrentSettingTurret";
const char* g_onSetModeX = "XStageMode";
const char* g_onSetModeY = "YStageMode";
const char* g_onSetModeZ = "ZStageMode";
const char* g_onSetModeT = "TurretMode";

// VulanScope Lamp
const char* g_OnLedIllumination = "SetLedBrightness";
const char* g_OnOneXLedIllumination = "SetOneXLedBrightness";
const char* g_OnSetLedPower = "SetLedPower";

// JoyStick
const char* g_OnEnableJoyStick = "EnableJoyStick";
const char* g_OnSpeedJoyStick = "SpeedJoyStick";

// ObjLens
const char* g_OnSetObj = "SetObjectiveLens";

// Global hub object for resource managment
VulanScopeHub g_hub_obj;

extern void sleepcp(int milli);

///////////////////////////////////////////////////////////////////////////////
// Exported MMDevice API
///////////////////////////////////////////////////////////////////////////////

MODULE_API	void InitializeModuleData()
{
	RegisterDevice(g_VulanScopeHub, MM::GenericDevice, "VulanScope Hub", true);
	RegisterDevice(g_VulanScopeXYDrive, MM::XYStageDevice, "XY Stage");
	RegisterDevice(g_VulanScopeZDrive, MM::StageDevice, "Z Stage");
	RegisterDevice(g_VulanScopeXYZDrive, MM::GenericDevice, "XYZ Com");
	RegisterDevice(g_VulanScopeLamp, MM::ShutterDevice, "VulanScope Lamp");
	RegisterDevice(g_VulanScopeObjLens, MM::StateDevice, "VulanScope Objective Lens");
	RegisterDevice(g_VulanScopeJoyStick, MM::GenericDevice, "VulanScope JoyStick");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0)
		return 0;

	if (strcmp(deviceName, g_VulanScopeHub) == 0)
	{
		return new Hub();
	}
	else if (strcmp(deviceName, g_VulanScopeXYDrive) == 0)
	{
		return new XYStage();
	}
	else if (strcmp(deviceName, g_VulanScopeZDrive) == 0)
	{
		return new ZStage();
	}
	else if (strcmp(deviceName, g_VulanScopeXYZDrive) == 0)
	{
		return new XYZCom();
	}
	else if (strcmp(deviceName, g_VulanScopeLamp) == 0)
	{
		return new Lamp();
	}
	else if (strcmp(deviceName, g_VulanScopeObjLens) == 0)
	{
		return new ObjLens();
	}
	else if (strcmp(deviceName, g_VulanScopeJoyStick) == 0)
	{
		return new JoyStick();
	}
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
	delete pDevice;
}

///////////////////////////////////////////////////////////////////////////////
// VulanScope Hub
///////////////////////////////////////////////////////////////////////////////

Hub::Hub() :
	initialized_(false),
	port_("Undefined")
{
	InitializeDefaultErrorMessages();

	// custom error messages
	SetErrorText(ERR_COMMAND_CANNOT_EXECUTE, "Command cannot be executed");
	SetErrorText(ERR_NO_ANSWER, "No answer received.  Is Vulcan connected to the correct serial port and switched on?");
    SetErrorText(ERR_NOT_CONNECTED, "No answer received.  Is Vulcan connected to the correct serial port and switched on?");

	// Create Pre-initialized Properties

	// Port
	CPropertyAction* pAct = new CPropertyAction(this, &Hub::OnPort);
	CreateProperty(MM::g_Keyword_Port, "Undefined", MM::String, false, pAct, true);
}

Hub::~Hub()
{
	Shutdown();
}

void Hub::GetName(char* name) const
{
	CDeviceUtils::CopyLimitedString(name, g_VulanScopeHub);
}

bool Hub::Busy()
{
	return false;
}

int Hub::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized())
	{
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}

	// set property list
	// -----------------

	// Name
	ret = CreateProperty(MM::g_Keyword_Name, g_VulanScopeHub, MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Description
	ret = CreateProperty(MM::g_Keyword_Description, "VulanScope controller", MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Version
	std::string version = g_hub_obj.Version();
	ret = CreateProperty("Firmware version", version.c_str(), MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	ret = UpdateStatus();
	if (ret != DEVICE_OK)
		return ret;

	initialized_ = true;

	return DEVICE_OK;
}

int Hub::Shutdown()
{
	if (initialized_)
	{
		initialized_ = false;
		g_hub_obj.DeInitialize();
	}

	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Action handlers
///////////////////////////////////////////////////////////////////////////////
/*
* Sets the Serial Port to be used.
* Should be called before initialization
*/
int Hub::OnPort(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set(port_.c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		if (initialized_)
		{
			// revert
			pProp->Set(port_.c_str());
			//return ERR_PORT_CHANGE_FORBIDDEN;
		}

		pProp->Get(port_);
		g_hub_obj.SetPort(port_.c_str());
	}
	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// VulanScope XYStage 
///////////////////////////////////////////////////////////////////////////////
XYStage::XYStage() :
	CXYStageBase<XYStage>(),
	initialized_(false),
	curSpeed_(2000),
	rampSpeed_(2000),
	curXSteps_(0),
	curYSteps_(0),
	limitYSteps_(0),
	limitXSteps_(0),
	xTrigger_(0),
	yTrigger_(0),
	getEnableNewAcqTrigger_(0),
    XYtriggerPreDelay_(0),
    XYtriggerPostDelay_(0),
    ZtriggerPreDelay_(0),
    ZtriggerPostDelay_(0),
    SetPulseDurationX_(500),
    SetPulseDurationY_(500),
    SetPulseDurationZ_(500),
	getEnableTrigger_(0),
    getEnableJS_(0),
  name_(g_VulanScopeXYDrive),
  command_("0,"),
  zStackTable_("0,")
{
	InitializeDefaultErrorMessages();
	SetErrorText(ERR_DEVICE_NOT_FOUND, "No XY-Drive found in this microscope");
	SetErrorText(ERR_PORT_NOT_SET, "No serial port found.  Did you include the VulanScope stage and set its serial port property?");

    std::cout << "Enable JS: " << getEnableJS_ << std::endl;
    std::cout << "command: " << command_ << std::endl;

//	std::cout << "VulanScope Version : Constant Zoffset, Auto Home" ;
//	std::cout << "HighSpeed USB Tranfser." << std::endl;

	// convert , steps to um. (one step is how many um)
	
}

XYStage::~XYStage()
{
	Shutdown();
}

bool XYStage::Busy()
{
	double posX1, posX2, posY1, posY2;
	int ret;

	ret = GetPositionUm(posX1, posY1);
	if (ret != DEVICE_OK)
		return true;

	sleepcp(10);

	ret = GetPositionUm(posX2, posY2);
	if (ret != DEVICE_OK)
		return true;

	if ((posX1 == posX2) && (posY1 == posY2))
		return false;
	else
		return true;
}

void XYStage::GetName(char* name) const
{
	assert(name_.length() < CDeviceUtils::GetMaxStringLength());
	CDeviceUtils::CopyLimitedString(name, name_.c_str());
}

int XYStage::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized()) {
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}

	// Name
	ret = CreateProperty(MM::g_Keyword_Name, name_.c_str(), MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Description
	ret = CreateProperty(MM::g_Keyword_Description, g_VulanScopeXYDrive, MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	//Action handlers
	/**********************************************************************************************
	* On Speed
	* Property Name : "SpeedXY"
	* Prop value range : 0 - 50 mm
	* Non Action Property
	*/
	CPropertyAction *pAct = new CPropertyAction(this, &XYStage::onSpeedXY);
	ret = CreateProperty(g_OnSpeedXY, "1.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSpeed = 0.0;
	float maxSpeed = 50;

	SetPropertyLimits(g_OnSpeedXY, minSpeed, maxSpeed);

	/**********************************************************************************************
	* On Ramp
	* Property Name : "RampXY"
	* Prop value range : 0 - 5000 steps / sec
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onRampXY);
	ret = CreateProperty(g_OnRampXY, "5000.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minRamp = 0.0;
	float maxRamp = 5000.0;

	SetPropertyLimits(g_OnRampXY, minRamp, maxRamp);

	/**********************************************************************************************
	* On Direction X
	* Property Name : "DirectionX"
	* Prop value : 0(Towards Home) or 1(Away From Home)
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onDirectionX);
	ret = CreateProperty(g_OnDirectionX, "1", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minDirX = 0;
	int maxDirX = 1;
	SetPropertyLimits(g_OnDirectionX, minDirX, maxDirX);

	/**********************************************************************************************
	* On Direction Y
	* Property Name : "DirectionY"
	* Prop value : 0(Towards Home) or 1(Away From Home
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onDirectionY);
	ret = CreateProperty(g_OnDirectionY, "1", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minDirY = 0;
	int maxDirY = 1;
	SetPropertyLimits(g_OnDirectionY, minDirY, maxDirY);

	/**********************************************************************************************
	* On xTrigger
	* Property Name : "XTrigger"
	* Prop value : 0 - 10,000 in steps
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onXTrigger);
	ret = CreateProperty(g_OnXTrigger, "1", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minXTrigger = 0;
	int maxXTrigger = 10000;
	SetPropertyLimits(g_OnXTrigger, minXTrigger, maxXTrigger);

	/**********************************************************************************************
    * On yTrigger
	* Property Name : "YTrigger"
	* Prop value : 0 - 10,000 in steps
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onYTrigger);
	ret = CreateProperty(g_OnYTrigger, "1", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minYTrigger = 0;
	int maxYTrigger = 10000;
	SetPropertyLimits(g_OnYTrigger, minYTrigger, maxYTrigger);
	
    /**********************************************************************************************
    * On xyTriggerPostDelay
    * Property Name : "XYTriggerPostDelay"
    * Prop value : 0 - 2000 in 1/seconds
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onTriggerXYPostDelay);
    ret = CreateProperty(g_OnXYTriggerPostDelay, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minXYTriggerPostDelayY = 0;
    int maxXYTriggerPostDelayY = 2000;
    SetPropertyLimits(g_OnXYTriggerPostDelay, minXYTriggerPostDelayY, maxXYTriggerPostDelayY);


    /**********************************************************************************************
    * On xyTriggerPreDelay
    * Property Name : "XYTriggerPreDelay"
    * Prop value : 0 - 2000 in 1/seconds
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onTriggerXYPreDelay);
    ret = CreateProperty(g_OnXYTriggerPreDelay, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minXYTriggerPreDelayX = 0;
    int maxXYTriggerPreDelayX = 2000;
    SetPropertyLimits(g_OnXYTriggerPreDelay, minXYTriggerPreDelayX, maxXYTriggerPreDelayX);


    /**********************************************************************************************
    * On zTriggerPreDelay
    * Property Name : "ZTriggerPreDelay"
    * Prop value : 0 - 2000 in 1/seconds
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onTriggerZPreDelay);
    ret = CreateProperty(g_OnZTriggerPreDelay, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZTriggerPreDelayY = 0;
    int maxZTriggerPreDelayY = 2000;
    SetPropertyLimits(g_OnZTriggerPreDelay, minZTriggerPreDelayY, maxZTriggerPreDelayY);


    /**********************************************************************************************
    * On zTriggerPostDelay
    * Property Name : "ZTriggerPostDelay"
    * Prop value : 0 - 2000 in 1/seconds
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onTriggerZPostDelay);
    ret = CreateProperty(g_OnZTriggerPostDelay, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZTriggerPostDelayY = 0;
    int maxZTriggerPostDelayY = 2000;
    SetPropertyLimits(g_OnZTriggerPostDelay, minZTriggerPostDelayY, maxZTriggerPostDelayY);

    /**********************************************************************************************
    * On SetPulseDurationX
    * Property Name : "SetPulseDurationX"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onSetPulseDurationX);
    ret = CreateProperty(g_OnSetPulseDurationX, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minSetPulseDurationX = 0;
    int maxSetPulseDurationX = 10000;
    SetPropertyLimits(g_OnSetPulseDurationX, minSetPulseDurationX, maxSetPulseDurationX);

    /**********************************************************************************************
    * On SetPulseDurationY
    * Property Name : "SetPulseDurationY"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onSetPulseDurationY);
    ret = CreateProperty(g_OnSetPulseDurationY, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minSetPulseDurationY = 0;
    int maxSetPulseDurationY = 10000;
    SetPropertyLimits(g_OnSetPulseDurationY, minSetPulseDurationY, maxSetPulseDurationY);

    /**********************************************************************************************
    * On SetPulseDurationZ
    * Property Name : "SetPulseDurationZ"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onSetPulseDurationZ);
    ret = CreateProperty(g_OnSetPulseDurationZ, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minSetPulseDurationZ = 0;
    int maxSetPulseDurationZ = 10000;
    SetPropertyLimits(g_OnSetPulseDurationZ, minSetPulseDurationZ, maxSetPulseDurationZ);

    /**********************************************************************************************
    * On EnableTrigger
    * Property Name : "EnableTrigger"
    * Prop value : ON, OFF
    * Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onEnableCommandLineAccess);
    ret = CreateProperty(g_OnEnableCommandLine, "OFF", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> EnableCommandLineValues;
    EnableCommandLineValues.push_back("ON");
    EnableCommandLineValues.push_back("OFF");

    ret = SetAllowedValues(g_OnEnableCommandLine, EnableCommandLineValues);

    /**********************************************************************************************
	* On EnableTrigger
	* Property Name : "EnableTrigger"
	* Prop value : ON, OFF
	* Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onEnableTrigger);
	ret = CreateProperty(g_OnEnableTrigger, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> EnableTrigValues;
	EnableTrigValues.push_back("ON");
	EnableTrigValues.push_back("OFF");

	ret = SetAllowedValues(g_OnEnableTrigger, EnableTrigValues);

    /**********************************************************************************************
    * On EnableTrigger
    * Property Name : "EnableTrigger"
    * Prop value : ON, OFF
    * Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onEnableNewAcqTrigger);
    ret = CreateProperty(g_OnEnableNewAcqTrigger, "OFF", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> EnableNewAcqTrigValues;
    EnableNewAcqTrigValues.push_back("ON");
    EnableNewAcqTrigValues.push_back("OFF");

    ret = SetAllowedValues(g_OnEnableNewAcqTrigger, EnableNewAcqTrigValues);

    /**********************************************************************************************
    * On SetZStackTable
    * Property Name : "SetZStackTable"
    * Prop value : any String input , eg(3,5,5,5,4,0,0,1,0), stack count, no. of relative Z-steps to capture
    * each image, total image count, followed by 0 to capture a stack and 1 for not capturing.
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onSendDesiredCommand);
    ret = CreateProperty(g_OnSendDesiredCommand, "0,", MM::String, false, pAct);
    assert(ret == DEVICE_OK);
    std::cout << "commandProp: " << command_ << std::endl;
    ClearAllowedValues(g_OnSendDesiredCommand);

    /**********************************************************************************************
    * On SetZStackTable
    * Property Name : "SetZStackTable"
    * Prop value : any String input , eg(3,5,5,5,4,0,0,1,0), stack count, no. of relative Z-steps to capture
    * each image, total image count, followed by 0 to capture a stack and 1 for not capturing.
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &XYStage::onSetZStackTable);
    ret = CreateProperty(g_OnSetZStackTable, "0,", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    ClearAllowedValues(g_OnSetZStack);

    // Action Handlers
	/**********************************************************************************************
	* JoyStick Enable
	* Property Name : "EnableJoyStick"
	* Prop value : 0(Enable) or 1(disable)
	* Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onEnableJS);
	ret = CreateProperty(g_OnEnableJoyStick, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> enableJS;
	enableJS.push_back("ON");
	enableJS.push_back("OFF");

	ret = SetAllowedValues(g_OnEnableJoyStick, enableJS);

	/**********************************************************************************************
	* On JoyStick Speed
	* Property Name : "SpeedJoyStick"
	* Prop value range : 0 - 5000 steps/ sec
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYStage::onSpeedJS);
	ret = CreateProperty(g_OnSpeedJoyStick, "200.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSpeedJS = 0.0;
	float maxSpeedJS = 5000.0;

	SetPropertyLimits(g_OnSpeedJoyStick, minSpeedJS, maxSpeedJS);

	/**********************************************************************************************
	* On GetStepSizeXYUm
	* Property Name : "GetStepSizeXYUm"
	* Prop value : 0 - 100 um
	* Non Action Property, Read only
	*/
	pAct = new CPropertyAction(this, &XYStage::onGetStepSizeXYUm);
	ret = CreateProperty(g_OnGetStepSizeXYUm, "1", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	int minGetStepSizeXYUm = 0;
	int maxGetStepSizeXYUm = 100;
	SetPropertyLimits(g_OnGetStepSizeXYUm, minGetStepSizeXYUm, maxGetStepSizeXYUm);

	initialized_ = true;

	return DEVICE_OK;
}

int XYStage::Shutdown()
{
	if (initialized_)
		initialized_ = false;

	return DEVICE_OK;
}

int XYStage::SetPositionUm(double xUm, double yUm)
{
	int ret;
	char buff[100];
	std::string respose;

        //std::cout << "X Passed: " << xUm << std::endl;
        //std::cout << "Y Passed: " << yUm << std::endl;

	// If joyStick is enabled, do a getPosition, This is should be done for restart also
	if (getEnableJS_ == 1 || getEnableJS_ == 0 || JoyStick::JoyStickEnabled_ == true )
	{
		double xJsPos;
		double yJsPos;

		GetPositionUm(xJsPos, yJsPos); // Get the current position after joyStick was used 
		//std::cout << "X pos :" << xJsPos << std::endl;
		//std::cout << "Y Pos :" << yJsPos << std::endl;
		//if (xJsPos == 0 && yJsPos == 0) // Keep this if case commented, as the Home sensors for XY are not Present 
		//{
		//	Home(); // if the stage was not sent to home , Internal check for home cmd.
		//	std::cout << "System Restarted, Home Command Executed.." << std::endl;
		//}
		microStepsInUmX_ = getMicroStepsInUmX_();
		microStepsInUmY_ = getMicroStepsInUmY_();

        curXSteps_ = static_cast<long int>(xJsPos / microStepsInUmX_); // convert it to steps
        curYSteps_ = static_cast<long int>(yJsPos / microStepsInUmY_);
	
        //std::cout << "cur x steps: " << curXSteps_ << std::endl;
        //std::cout << "cur y steps: " << curYSteps_ << std::endl;

		getEnableJS_ = 2;
		JoyStick::JoyStickEnabled_ = false;
	}

	// Get MicroSteps  in Um
	microStepsInUmX_ = getMicroStepsInUmX_();
	microStepsInUmY_ = getMicroStepsInUmY_();

	// X axis movement implemented with auto X direction  
    long int positionX = static_cast<long int>(xUm / microStepsInUmX_); // Convert um to steps

	long int x_steps = (std::abs(positionX) - curXSteps_); // New Position to move in steps
	//std::cout << "Xsteps to be Moved : " << x_steps << std::endl;
	if (x_steps <= 0)
		xDirection_ = 0; // Negative number leads to move towards Home
	else
		xDirection_ = 1; // Move Away home if Positive 

	// Update check 1.0
	curXSteps_ = std::abs(positionX);

	// Upper Limit Check
	if (curXSteps_ > XYZCom::limitX_)
	{
		curXSteps_ = XYZCom::limitX_;
		std::cout << "Upper Limit X, Please change the upper Limit" << std::endl;
	}

	if (curXSteps_ < 0)
	{
		curXSteps_ = 0;
		std::cout << "Negative Value hit" << std::endl;
	}

    long int positionY = static_cast<long int>(yUm / microStepsInUmY_); // Convert um to steps

	long int Y_steps = (std::abs(positionY) - curYSteps_); // New Position to move in steps
	
	if (Y_steps <= 0)
		yDirection_ = 0; // Negative number leads to move towards Home
	else
		yDirection_ = 1; // Move Away home if Positive 

	curYSteps_ = std::abs(positionY);
	
	// Upper Limit check
	if (curYSteps_ > XYZCom::limitY_)
	{
    curYSteps_ = XYZCom::limitY_;
		std::cout << "Upper Limit Y, Please change the upper Limit" << std::endl;
	}

	if (curYSteps_ < 0)
	{
		curYSteps_ = 0;
		std::cout << "Negative Value hit" << std::endl;
	}

    snprintf(buff, sizeof(buff), "$170,14,%d,%d,%ld,%d,", int(curSpeed_), int(curSpeed_),std::abs(static_cast<long int>(x_steps)), int(xDirection_));
//    as  $170,14;3200;3200;4000;1;

	std::string SetXstr = buff;
	//std::cout << "Set X : " << SetXstr << std::endl;
	std::string SetXSerial = g_hub_obj.CrcCalculator(SetXstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

    snprintf(buff, sizeof(buff), "$170,15,%d,%d,%ld,%d,", int(curSpeed_), int(curSpeed_), std::abs(static_cast<long int>(Y_steps)), int(yDirection_));

	std::string SetYstr = buff;
        //std::cout << "Set Y : " << SetYstr << std::endl;
	std::string SetYSerial = g_hub_obj.CrcCalculator(SetYstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetYSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int XYStage::SetRelativePositionUm(double xUm, double yUm)
{
	int ret;
	char buff[100];
	std::string respose;

	// Get MicroSteps  in Um
	microStepsInUmX_ = getMicroStepsInUmX_();
	microStepsInUmY_ = getMicroStepsInUmY_();
	// X axis movement implemented with auto X direction  
	long int x_steps = static_cast<long int>(xUm / microStepsInUmX_); // Convert um to steps 

	if (x_steps <= 0)
		xDirection_ = 0; // Negative number leads to move towards Home
	else
		xDirection_ = 1; // Move Away home if Positive 

	curXSteps_ = std::abs(x_steps);

	/*snprintf(buff, sizeof(buff), "$170,2,%d,%d,0,%ld,0,0,0,%d,0,0,", int(curSpeed_), int(rampSpeed_), static_cast<long int>(curXSteps_), int(xDirection_));
	std::string SetXstr = buff;
	std::string SetXSerial = g_hub_obj.CrcCalculator(SetXstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXSerial, respose);
	if (ret != DEVICE_OK)
	return ret;
	*/
	// Y axis movement implemented with auto Y direction 
	long int Y_steps = static_cast<long int>(yUm / microStepsInUmY_); // Convert um to steps 

	if (Y_steps <= 0)
		yDirection_ = 0; // Negative number leads to move towards Home
	else
		yDirection_ = 1; // Move Away home if Positive 

	curYSteps_ = std::abs(Y_steps);

	snprintf(buff, sizeof(buff), "$170,2,%d,%d,0,%ld,%ld,0,0,%d,%d,0,", int(curSpeed_), int(curSpeed_), static_cast<long int>(curXSteps_), \
		static_cast<long int>(curYSteps_), int(xDirection_), int(yDirection_));
	std::string SetYstr = buff;
	std::string SetYSerial = g_hub_obj.CrcCalculator(SetYstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetYSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int XYStage::SetAdapterOriginUm(double xUm, double yUm)
{
	return DEVICE_OK;
}

int XYStage::GetPositionUm(double& posX, double& posY)
{
	int ret;
	std::string response;
	long x_steps;
	long y_steps;

	// Gets Response for XY Axiz

	std::string GetXYstr = "$170,7,";      // Get comamand
	std::string GetXYSerial = g_hub_obj.CrcCalculator(GetXYstr); // pass the command to CrcCal
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), GetXYSerial, response); // send to hub
	if (ret != DEVICE_OK) // check if the transfer of info was correct 
		return ret;

	std::stringstream ss(response);
	std::vector<std::string> result;
	std::string substr;

	while (ss.good())
	{
		getline(ss, substr, ','); // substring with ',' and store all the data in vector 
		result.push_back(substr);
	}
	if (result.size() < 8)
	{
		std::cout << "Vector mismatch XY Stage, getPostionUm" << std::endl;
		std::cout << "size : " << result.size() << std::endl;
		return	DEVICE_ERR;
	}
	x_steps = std::atoi(result[3].c_str());
	y_steps = std::atoi(result[4].c_str());

	microStepsInUmX_ = getMicroStepsInUmX_();
	microStepsInUmY_ = getMicroStepsInUmY_();

	posX = (x_steps * microStepsInUmX_); // Convert steps to microns 
	posY = (y_steps * microStepsInUmY_);

	return DEVICE_OK;
}

int XYStage::GetLimitsUm(double& xMinPosUm, double& xMaxPosUm, double& yMinPosUm, double& yMaxPosUm)
{
	return DEVICE_OK;
}

int XYStage::SetPositionSteps(long positionX, long positionY)
{
	int ret;
	char buff[100];
	std::string respose;

	// X axis movement implemented with auto X direction 
	long int x_steps = (std::abs(positionX) - curXSteps_); // New Position to move in steps

	if (x_steps <= 0) // Find the direction to Move 
		xDirection_ = 0; // Negative number leads to move towards Home
	else
		xDirection_ = 1; // Move Away home if Positive 

	curXSteps_ = std::abs(x_steps); // Store the position, For next movement

	snprintf(buff, sizeof(buff), "$170,2,%d,%d,0,%ld,0,0,0,%d,0,0,", int(curSpeed_), int(curSpeed_), static_cast<long int>(curXSteps_), int(xDirection_));
	std::string SetXstr = buff;
	std::string SetXSerial = g_hub_obj.CrcCalculator(SetXstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	// Y axis movement implemented with auto Y direction 
	long int Y_steps = (std::abs(positionY) - curYSteps_); // New Position to move in steps

	if (Y_steps <= 0)// Find the direction to Move 
		yDirection_ = 0; // Negative number leads to move towards Home
	else
		yDirection_ = 1; // Move Away home if Positive 

	curYSteps_ = std::abs(Y_steps); // Store the position, For next movement

	snprintf(buff, sizeof(buff), "$170,2,%d,%d,0,0,%ld,0,0,0,%d,0,", int(curSpeed_), int(curSpeed_), static_cast<long int>(curYSteps_), int(yDirection_));
	std::string SetYstr = buff;
	std::string SetYSerial = g_hub_obj.CrcCalculator(SetYstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetYSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int XYStage::GetPositionSteps(long& positionX, long& positionY)
{
	int ret;
	std::string response;
	long x_steps;
	long y_steps;

	// Gets Response for XY Axiz

	std::string GetXYstr = "$170,7,";      // Get comamand
	std::string GetXYSerial = g_hub_obj.CrcCalculator(GetXYstr); // pass the command to CrcCal
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), GetXYSerial, response); // send to hub
	if (ret != DEVICE_OK) // check if the transfer of info was correct 
		return ret;

	std::stringstream ss(response);
	std::vector<std::string> result;
	std::string substr;

	while (ss.good())
	{
		getline(ss, substr, ','); // substring with ',' and store all the data in vector 
		result.push_back(substr);
	}

	x_steps = std::atoi(result[3].c_str());
	y_steps = std::atoi(result[4].c_str());

	positionX = x_steps;
	positionY = y_steps;

	return DEVICE_OK;
}

int XYStage::SetRelativePositionSteps(long positionX, long positionY)
{
	int ret;
	char buff[100];
	std::string respose;

	// Implemented with auto X Direction
	long int x_steps = positionX;

	if (x_steps <= 0)
		xDirection_ = 0; // Negative number leads to move towards Home
	else
		xDirection_ = 1; // Move Away home if Positive 

	curXSteps_ = std::abs(x_steps); // Store current steps

	//snprintf(buff, sizeof(buff), "$170,2,%d,%d,0,%ld,0,0,0,%d,0,0,", int(curSpeed_), int(rampSpeed_), static_cast<long int>(curXSteps_), int(xDirection_));
	//std::string SetXstr = buff;
	//std::string SetXSerial = g_hub_obj.CrcCalculator(SetXstr);
	//ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXSerial, respose);
	//if (ret != DEVICE_OK)
	//	return ret;

// Implemented with auto Y Direction
	long int Y_steps = positionY; // New Position to move in steps
	if (Y_steps <= 0)
		yDirection_ = 0; // Negative number leads to move towards Home
	else
		yDirection_ = 1; // Move Away home if Positive 

	curYSteps_ = std::abs(Y_steps);

	snprintf(buff, sizeof(buff), "$170,2,%d,%d,0,%ld,%ld,0,0,%d,%d,0,", int(curSpeed_), int(curSpeed_), static_cast<long int>(curXSteps_), static_cast<long int>(curYSteps_), int(xDirection_), int(yDirection_));
	std::string SetYstr = buff;
	std::string SetYSerial = g_hub_obj.CrcCalculator(SetYstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetYSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int XYStage::XStageHome()
{
    int ret;
    std::string responseX;
    std::vector<std::string> getHomeX;
    std::string substr;
    int idx = 0;

    long homeXPos;   // Used for Adaptive Sleep check

    // Go to home at Max Speed
    long speedXHome = XYZCom::microStepX_ * 350;
    long speedYHome = XYZCom::microStepY_ * 350;

    if (speedXHome > 20000)
        speedXHome = 20000;
    if (speedYHome > 20000)
        speedYHome = 20000;


    curXSteps_ = 0;

    char buffX[100];

    // Sets the Limit for XYZ Stage , always do this at home
    snprintf(buffX, sizeof(buffX), "$170,4,%ld,%ld,0,1,0,0,", speedXHome, speedXHome);


    std::string HomeXStr = buffX;
    std::cout << "Home Speed check :" << HomeXStr << std::endl;
    std::string HomeXSerial = g_hub_obj.CrcCalculator(HomeXStr);

    ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), HomeXSerial, responseX);
    if (ret != DEVICE_OK)
        return ret;

    // Blocking call, waits till it reaches the stage. Max wait 5sec
    while (idx < 500)
    {
        idx++;
        std::string getHomeXStr = "$170,17,";
        std::string getHomeXSerial = g_hub_obj.CrcCalculator(getHomeXStr);
        ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getHomeXSerial, responseX);
        if (ret != DEVICE_OK)
            return ret;

        std::stringstream ss(responseX);

        while (ss.good())
        {
            getline(ss, substr, ','); // substring with ',' and store all the data in vector
            getHomeX.push_back(substr);
        }
        if (getHomeX.size() < 8)
        {
            std::cout << "Vector Size Mismatch From HomeXY " << std::endl;
            std::cout << "Size : " << getHomeX.size() << std::endl;
            return DEVICE_ERR;
        }

        homeXPos = std::atoi(getHomeX[3].c_str());
        //std::cout << "GetHomeX : " << homeXPos << std::endl;
        if (homeXPos != 0)
        {
            sleepcp(30);
            getHomeX.clear();
        }
        else
            break;
    }


    return DEVICE_OK;

    return DEVICE_OK;
}


int XYStage::Home()
{
	int ret;
	std::string responseX;
	std::string responseY;
	std::vector<std::string> getHomeX;
	std::vector<std::string> getHomeY;
	std::string substr;
	int idx = 0;
	int idy = 0;

	long homeXPos;   // Used for Adaptive Sleep check
	long homeYPos;

	// Go to home at Max Speed
	long speedXHome = XYZCom::microStepX_ * 350;
	long speedYHome = XYZCom::microStepY_ * 350;

	if (speedXHome > 20000)
		speedXHome = 20000;
	if (speedYHome > 20000)
		speedYHome = 20000;

	curXSteps_ = 0;
	curYSteps_ = 0;

	char buffY[100];

	// Sets the Limit for XYZ Stage , always do this at home 
	snprintf(buffY, sizeof(buffY), "$170,4,%ld,%ld,0,0,1,0,", speedYHome,speedYHome);
	
	std::string HomeYStr = buffY; // Y motor are moved to Home postion with Max speed 
	std::cout << "Home Speed check :" << HomeYStr << std::endl;

	std::string HomeYSerial = g_hub_obj.CrcCalculator(HomeYStr);

	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), HomeYSerial, responseY);
	if (ret != DEVICE_OK)
		return ret;

	// Blocking call, waits till it reaches the stage. Max wait 5sec
	while (idy < 500)
	{
		idy++;
		responseY = "";
		std::string getHomeYStr = "$170,17,";
		std::string getHomeYSerial = g_hub_obj.CrcCalculator(getHomeYStr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getHomeYSerial, responseY);
		if (ret != DEVICE_OK)
			return ret;

		std::stringstream ss(responseY);

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			getHomeY.push_back(substr);
		}
		if (getHomeY.size() < 8)
		{
			std::cout << "Vector Size Mismatch From HomeXY " << std::endl;
			std::cout << "Size : " << getHomeY.size() << std::endl;
			return DEVICE_ERR;
		}

		homeYPos = std::atoi(getHomeY[4].c_str());
		//std::cout << "GetHomeY : " << homeYPos << std::endl;
		if (homeYPos != 0)
		{
			sleepcp(30);
			getHomeY.clear();
		}
		else
			break;
	}


	char buffX[100];

	// Sets the Limit for XYZ Stage , always do this at home 
	snprintf(buffX, sizeof(buffX), "$170,4,%ld,%ld,0,1,0,0,", speedXHome, speedXHome);


	std::string HomeXStr = buffX;
	std::cout << "Home Speed check :" << HomeXStr << std::endl;
	std::string HomeXSerial = g_hub_obj.CrcCalculator(HomeXStr);

	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), HomeXSerial, responseX);
	if (ret != DEVICE_OK)
		return ret;

	// Blocking call, waits till it reaches the stage. Max wait 5sec
	while (idx < 500)
	{
		idx++;
		std::string getHomeXStr = "$170,17,";
		std::string getHomeXSerial = g_hub_obj.CrcCalculator(getHomeXStr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getHomeXSerial, responseX);
		if (ret != DEVICE_OK)
			return ret;

		std::stringstream ss(responseX);

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			getHomeX.push_back(substr);
		}
		if (getHomeX.size() < 8)
		{
			std::cout << "Vector Size Mismatch From HomeXY " << std::endl;
			std::cout << "Size : " << getHomeX.size() << std::endl;
			return DEVICE_ERR;
		}

		homeXPos = std::atoi(getHomeX[3].c_str());
		//std::cout << "GetHomeX : " << homeXPos << std::endl;
		if (homeXPos != 0)
		{
			sleepcp(30);
			getHomeX.clear();
		}
		else
			break;
	}


	return DEVICE_OK;

	return DEVICE_OK;
}

int XYStage::Stop()
{
	int ret;
	std::string response;

	std::string StopStr = "$170,1,1,1,0,0,0,"; // Disables XY motor, Both the motors have to selected again before use
	std::string StopSerial = g_hub_obj.CrcCalculator(StopStr);

	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), StopSerial, response);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int XYStage::SetOrigin()
{
	return DEVICE_OK;
}

int XYStage::GetStepLimits(long& xMin, long& xMax, long& yMin, long& yMax)
{
	return DEVICE_OK;
}

double XYStage::GetStepSizeXUm()
{
	return microStepsInUmX_; // 1 step in um
}

double XYStage::GetStepSizeYUm()
{
	return microStepsInUmY_; // 1 step in um
}

int XYStage::IsXYStageSequenceable(bool& isSequenceable) const
{
	isSequenceable = false;
	return DEVICE_OK;
}

double XYStage::getMicroStepsInUmX_()
{
	// code written for 10mm screw code 

	double screwRodUm = 10000; // Inum
	
	double retVal = double(screwRodUm / (XYZCom::microStepX_ * 200));
	//std::cout << "Xum : " << retVal << std::endl;

	return retVal;
}

double XYStage::getMicroStepsInUmY_()
{
	// code written for 10mm screw code 

	double screwRodUm = 10000; // Inum

	double retVal = double(screwRodUm / (XYZCom::microStepY_ * 200));
	//std::cout << "Yum : " << retVal << std::endl;

	return retVal;
}

///////////////////////////////////////////////////////////////////////////////
// Spin Scope Action handlers XY Stage
/******************************************************************************
* Sets the Speed of the XY Stage
* Values : 0.0 - 5000 steps/sec
* Default Value : 2000
* Non Action Property
*/
int XYStage::onSpeedXY(MM::PropertyBase* prop, MM::ActionType eact)
{
	double val;
	microStepsInUmY_ = getMicroStepsInUmY_();
	if (eact == MM::AfterSet)
	{
		prop->Get(val);
		curSpeed_ = static_cast<long int>(floor((val / (microStepsInUmY_/ 1000)))); // Convert mm to steps  
	}
	else if (eact == MM::BeforeGet)
	{
        val = (curSpeed_ * (microStepsInUmY_ / 1000)); // convert steps to mm
		prop->Set(val);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the Acceleration of the XY Stage
* Values : 0.0 - 5000 Steps/sec
* Default Value : 2000
* Non Action Property
*/
int XYStage::onRampXY(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		prop->Get(rampSpeed_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(rampSpeed_);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the Direction of X satge
* Values :  0(Towards Home) - 1(Away From Home)
* Default Value : 1
* Non Action Property
*/
int XYStage::onDirectionX(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		prop->Get(xDirection_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(xDirection_);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the Direction of Y satge
* Values :  0(Towards Home) - 1(Away From Home)
* Default Value : 1
* Non Action Property
*/
int XYStage::onDirectionY(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		prop->Get(yDirection_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(yDirection_);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 10,000 steps
* Default Value : 0
* Action Property
*/
int XYStage::onXTrigger(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
        long val;
		prop->Get(val);

		xTrigger_ = static_cast<long int>(val);
	}
	else if (eact == MM::BeforeGet)
	{
		long val;
		val = xTrigger_;
		prop->Set(val);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onTriggerXYPostDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);

        XYtriggerPostDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = XYtriggerPostDelay_;
        prop->Set(val);
    }
    return DEVICE_OK;
}



/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onTriggerXYPreDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);

        XYtriggerPreDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = XYtriggerPreDelay_;
        prop->Set(val);
    }
    return DEVICE_OK;
}


/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onTriggerZPostDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);

        ZtriggerPostDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = ZtriggerPostDelay_;
        prop->Set(val);
    }
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger pulse duration values in setps, pulse is created every Xsteps
* Values :  0  - 10000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onSetPulseDurationX(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached SetPulseDurationX_"<<std::endl;
        long val;
        prop->Get(val);

        SetPulseDurationX_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = SetPulseDurationX_;
        prop->Set(val);
    }
//    std::cout<<"set SetPulseDurationX_"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  YTrigger pulse duration values in setps, pulse is created every Xsteps
* Values :  0  - 10000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onSetPulseDurationY(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached SetPulseDurationY_"<<std::endl;
        long val;
        prop->Get(val);

        SetPulseDurationY_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = SetPulseDurationY_;
        prop->Set(val);
    }
//    std::cout<<"set SetPulseDurationY_"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  ZTrigger pulse duration values in setps, pulse is created every Xsteps
* Values :  0  - 10000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onSetPulseDurationZ(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached SetPulseDurationZ_"<<std::endl;
        long val;
        prop->Get(val);

        SetPulseDurationZ_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = SetPulseDurationZ_;
        prop->Set(val);
    }
//    std::cout<<"set SetPulseDurationZ_"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int XYStage::onTriggerZPreDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);

        ZtriggerPreDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = ZtriggerPreDelay_;
        prop->Set(val);
    }
    return DEVICE_OK;
}


/******************************************************************************
* Sets the  YTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 10,000 steps
* Default Value : 0
* Action Property
*/
int XYStage::onYTrigger(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		long val;
        prop->Get(val);

		yTrigger_ = static_cast<long int>(val);
	}
	else if (eact == MM::BeforeGet)
	{
		long val;
		val = yTrigger_;
		prop->Set(val);
	}
	return DEVICE_OK;
}

/******************************************************************************
* send whatever command we pass from the command line to the controller
* Values :  command to the controller
* Default Value : 0
* Action Property
*/
int XYStage::onSendDesiredCommand(MM::PropertyBase* prop, MM::ActionType eact)
{
  if (eact == MM::AfterSet)
  {
    std::cout << "commandPropGet: " << command_ << std::endl;
    prop->Get(command_);
  }
  else if (eact == MM::BeforeGet)
  {
    prop->Set(command_.c_str());
  }
  return DEVICE_OK;
}

/******************************************************************************
* Sets the Z-Table for Z-Trigger capture during XY continuous acquisition.
* Values :  Stack count, step size, Z-Stack table.
* Default Value : 0
* Action Property
*/
int XYStage::onSetZStackTable(MM::PropertyBase* prop, MM::ActionType eact)
{
  if (eact == MM::AfterSet)
  {
    prop->Get(zStackTable_);
  }
  else if (eact == MM::BeforeGet)
  {
    prop->Set(zStackTable_.c_str());
  }
  return DEVICE_OK;
}

/******************************************************************************
 * sets the new acquisition pattern trigger mode. If x moves by the given step
 * size the z trigger would get initiated.
 * Values : ON, OFF
 * Default Value : OFF
 * Action Property
 */
int XYStage::onEnableNewAcqTrigger(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        int ret;
        std::string Val;
        prop->Get(Val);

        if (strcmp(Val.c_str(), "ON")==0)
        {
          // create a vector of string  from string passed
          std::stringstream ssToVec(zStackTable_);
          std::vector<std::string> stringVec;
          std::string substr;
          while (ssToVec.good())
          {
              getline(ssToVec, substr, ';'); // substring with ',' and store all the data in vector
              stringVec.push_back(substr);
          }

          // convert string vector to double vector
          std::vector <long> lVec(stringVec.size());
          std::transform(stringVec.begin(), stringVec.end(), lVec.begin(), [](const std::string& Tval)
          {
              return std::stol(Tval);
          });

          std::string setZStackStr;
          char buff[1000];
          snprintf(buff, sizeof(buff), "$170,27,");
          setZStackStr = buff;

          std::stringstream ss;
          std::copy(lVec.begin(), lVec.end(), std::ostream_iterator<long int>(ss, ","));
          setZStackStr += ss.str();
//          setZStackStr += ",";

//            char buff[100];
            getEnableNewAcqTrigger_ = 1;
            std::string response;
//            snprintf(buff, sizeof(buff), "$170,27,5,1005,5,5,5,5,");
//            snprintf(buff, sizeof(buff), "$170,27,5,1005,5,5,5,5,10,0,0,0,0,0,0,0,0,0,0,");
//            snprintf(buff, sizeof(buff), "$170,27,%s", zStackTable_.c_str());
            std::string EnableTriggerStr = setZStackStr;
            std::string EnableTriggerSerial = g_hub_obj.CrcCalculator(EnableTriggerStr);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableTriggerSerial, response);
            if (ret != DEVICE_OK)
                return ret;
        }
        else if (strcmp(Val.c_str(), "OFF") == 0)
        {
            getEnableNewAcqTrigger_ = 0;
            std::string response;
            std::string EnableLedStr = "$170,27,0,";
            std::string EnableLedSerial = g_hub_obj.CrcCalculator(EnableLedStr);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableLedSerial, response);
            if (ret != DEVICE_OK)
                return ret;
        }
        else
            assert(false); // This should never happen

        return DEVICE_OK;
    }
    if (eact == MM::BeforeGet)
    {
        if (getEnableNewAcqTrigger_ == 0)
        {
            prop->Set("OFF");
        }
        else if (getEnableNewAcqTrigger_ == 1)
        {
            prop->Set("ON");
        }
    }
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  YTrigger,Xtrigger .If Either one moves trigger is created of
* of a small pulse width of 0.01sec(HardCoded value)
* For LabEn adapter, Switch X and Y internally
* Values :  ON, OFF
* Default Value : OFF
* Action Property
*/
int XYStage::onEnableCommandLineAccess(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        int ret;
        std::string val;
        prop->Get(val);

        if (strcmp(val.c_str(), "ON") == 0)
        {
            std::stringstream ssToVec(command_);
            std::vector<std::string> stringVec;
            std::string substr;
            while (ssToVec.good())
            {
                getline(ssToVec, substr, ';'); // substring with ',' and store all the data in vector
                stringVec.push_back(substr);
            }

            // convert string vector to double vector
            std::vector <long> lVec(stringVec.size());
            std::transform(stringVec.begin(), stringVec.end(), lVec.begin(), [](const std::string& Tval)
            {
                return std::stol(Tval);
            });

            std::string commandString;
            char buff[1000];
            snprintf(buff, sizeof(buff), "$170,");
            commandString = buff;

            std::stringstream ss;
            std::copy(lVec.begin(), lVec.end(), std::ostream_iterator<long int>(ss, ","));
            commandString += ss.str();
            getEnableCommandLine_ = 1;
            std::string response;
            std::cout << "command within func: " << command_.c_str() << std::endl;
            std::string EnableCommandLineStr = commandString;
            std::cout << "command str: " << EnableCommandLineStr.c_str() << std::endl;
            std::string EnableCommandLineSerial = g_hub_obj.CrcCalculator(EnableCommandLineStr);
//			ret = g_hub_obj.SetSerialCommand(*this, *GetCoreCallback(), EnableTriggerSerial, response);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableCommandLineSerial, response);
            if (ret != DEVICE_OK)
                return ret;
        }
        else if (strcmp(val.c_str(), "OFF") == 0)
        {
            getEnableCommandLine_ = 0;
            ret = DEVICE_OK;
            if (ret != DEVICE_OK)
                return ret;
        }
        else
            assert(false); // This should never happen

        return DEVICE_OK;
    }

    if (eact == MM::BeforeGet)
    {
        if (getEnableCommandLine_ == 0)
        {
            prop->Set("OFF");
        }
        else if (getEnableCommandLine_ == 1)
        {
            prop->Set("ON");
        }
    }
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  YTrigger,Xtrigger .If Either one moves trigger is created of
* of a small pulse width of 0.01sec(HardCoded value)
* For LabEn adapter, Switch X and Y internally
* Values :  ON, OFF
* Default Value : OFF
* Action Property
*/
int XYStage::onEnableTrigger(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		int ret;
		std::string val;
		prop->Get(val);

		if (strcmp(val.c_str(), "ON") == 0)
		{
			char buff[100];
			getEnableTrigger_ = 1;
			std::string response;
      snprintf(buff, sizeof(buff), "$170,9,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,", xTrigger_, yTrigger_,0,SetPulseDurationX_,SetPulseDurationY_,SetPulseDurationZ_,XYtriggerPostDelay_,ZtriggerPostDelay_,XYtriggerPreDelay_,ZtriggerPreDelay_);
//            snprintf(buff, sizeof(buff), "$170,9,%d,%d,%d,100,100,500,%d,%d,%d,%d,", xTrigger_, yTrigger_,0,XYtriggerPostDelay_,ZtriggerPostDelay_,XYtriggerPreDelay_,ZtriggerPreDelay_);
//            snprintf(buff, sizeof(buff), "$170,9,%d,%d,%d,100,100,500,%d,%d,%d,%d,", xTrigger_, yTrigger_,0,0,0,0,0);
			std::string EnableTriggerStr = buff;
			std::string EnableTriggerSerial = g_hub_obj.CrcCalculator(EnableTriggerStr);
//			ret = g_hub_obj.SetSerialCommand(*this, *GetCoreCallback(), EnableTriggerSerial, response);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableTriggerSerial, response);
			if (ret != DEVICE_OK)
				return ret;
		}
		else if (strcmp(val.c_str(), "OFF") == 0)
		{
			getEnableTrigger_ = 0;
			xTrigger_ = 0;
			yTrigger_ = 0;
			std::string response;
            std::string EnableLedStr = "$170,9,0,0,0,0,0,0,0,0,0,0,";
            std::string EnableLedSerial = g_hub_obj.CrcCalculator(EnableLedStr);
//			ret = g_hub_obj.SetSerialCommand(*this, *GetCoreCallback(), EnableLedSerial, response);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableLedSerial, response);
			if (ret != DEVICE_OK)
				return ret;
		}
		else
			assert(false); // This should never happen 

		return DEVICE_OK;
	}

	if (eact == MM::BeforeGet)
	{
		if (getEnableTrigger_ == 0)
		{
			prop->Set("OFF");
		}
		else if (getEnableTrigger_ == 1)
		{
			prop->Set("ON");
		}
	}
	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Spin Scope Action handlers JoyStick
/******************************************************************************
* Used to enable JoyStick
* Values :  ON , OFF
* Default Value : OFF
* 0 is enable, 1 is disable
* Action Property
*/
int XYStage::onEnableJS(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		char buff[100];
		int ret;
		std::string val;
		prop->Get(val);

		if (strcmp(val.c_str(), "ON") == 0)
		{
			getEnableJS_ = 1;
			std::string response;

			snprintf(buff, sizeof(buff), "$170,12,%ld,%ld,1,", speedJS_, speedJS_);
			std::string EnableJSStr = buff;
			std::string EnableJSSerial = g_hub_obj.CrcCalculator(EnableJSStr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableJSSerial, response);
		}
		else if (strcmp(val.c_str(), "OFF") == 0)
		{
			getEnableJS_ = 0;
			std::string response;
			std::string DisableJSStr = "$170,13,0,0,0,0,";
			std::string DisableJSSerial = g_hub_obj.CrcCalculator(DisableJSStr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), DisableJSSerial, response);

			// Pass the new position to SetPosition


		}
		if (ret != DEVICE_OK)
			return ret;

	}
	else if (eact == MM::BeforeGet)
	{

		if (getEnableJS_ == 1)
		{
			prop->Set("ON");
		}
		else if (getEnableJS_ == 0)
		{
			prop->Set("OFF");
		}
	}

	return DEVICE_OK;
}

/******************************************************************************
* Sets the Speed of the Joy Stick
* Values : 0.0 - 5000 steps/sec
* Default Value : 200
* Non Action Property
*/
int XYStage::onSpeedJS(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		prop->Get(speedJS_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(speedJS_);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Gets the  GetStep size in um for XY, Read Only
* Values :  0  - 100 um
* Non Action Property
*/
int XYStage::onGetStepSizeXYUm(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		// DUMMY Set, value Unused
	}
	else if (eact == MM::BeforeGet)
	{
		double val;
		microStepsInUmY_ = getMicroStepsInUmY_();
		val = microStepsInUmY_;
		std::cout<<"microStepsInUmY_: "<< microStepsInUmY_<<std::endl;
		//val = 4;
		prop->Set(val);
	}
	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
//  VulanScope Z-Stage
///////////////////////////////////////////////////////////////////////////////
ZStage::ZStage() :
	CStageBase<ZStage>(),
	initialized_(false),
	curSpeedZ_(5000),
	rampSpeedZ_(5000),
	curZSteps_(0),
	backlashZ_(0),
	restart_(true),
    XYtriggerPreDelay_(0),
    XYtriggerPostDelay_(0),
    ZtriggerPreDelay_(0),
    ZtriggerPostDelay_(0),
    ZStackSetInstance_(0),
    SetPulseDurationX_(500),
    SetPulseDurationY_(500),
    SetPulseDurationZ_(500),
	homeCounter_(0),
    getEnableTrigger_(0),
    zStackYSteps_(0),
     zStackXSteps_(0),
  name_(g_VulanScopeZDrive),
  zStackSize_(0),
  zStackStepSize_(0.0)
{
	InitializeDefaultErrorMessages();
	SetErrorText(ERR_DEVICE_NOT_FOUND, "No Z-Stage Found in this microscope");
	SetErrorText(ERR_PORT_NOT_SET, "No Serial port found ");

	// microsteps conversion to um, one steps is how many Um
	microStepsInUmZ_ = 0.05;
}

ZStage::~ZStage()
{
	Shutdown();
}

bool ZStage::Busy()
{
	double posZ1, posZ2;
	int ret;

	ret = GetPositionUm(posZ1);
	if (ret != DEVICE_OK)
		return ret;

	sleepcp(10);

	ret = GetPositionUm(posZ2);
	if (ret != DEVICE_OK)
		return ret;
	//std::cout << "Pos 1 : " << posZ1 << "\tPos 2 :" << posZ2 << std::endl;

	if (posZ1 == posZ2)
		return false;
	else
		return true;
}

void ZStage::GetName(char* name) const
{
	CDeviceUtils::CopyLimitedString(name, name_.c_str());
}

int ZStage::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized())
	{
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}

	// Name
	ret = CreateProperty(MM::g_Keyword_Name, name_.c_str(), MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Description
	ret = CreateProperty(MM::g_Keyword_Description, g_VulanScopeZDrive, MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	//Action handlers
	/**********************************************************************************************
	* On Speed
	* Property Name : "SpeedZ"
	* Prop value range : 0 - 1 mm per sec
	* Non Action Property
	*/
	CPropertyAction *pAct = new CPropertyAction(this, &ZStage::onSpeedZ);
	ret = CreateProperty(g_OnSpeedZ, "0.25", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSpeed = 0.0;
	float maxSpeed = 10;

	SetPropertyLimits(g_OnSpeedZ, minSpeed, maxSpeed);

	/**********************************************************************************************
	* On Ramp
	* Property Name : "RampZ"
	* Prop value range : 0 - 10000 steps / sec
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &ZStage::onRampZ);
	ret = CreateProperty(g_OnRampZ, "5000.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minRampZ = 0.0;
	float maxRampZ = 5000.0;

	SetPropertyLimits(g_OnRampZ, minRampZ, maxRampZ);

	/**********************************************************************************************
	* On Direction Z
	* Property Name : "DirectionZ"
	* Prop value : 0(Towards Home) or 1(Away From Home)
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &ZStage::onDirectionZ);
	ret = CreateProperty(g_OnDirectionZ, "1", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minDirZ = 0;
	int maxDirZ = 1;
	SetPropertyLimits(g_OnDirectionZ, minDirZ, maxDirZ);

	/**********************************************************************************************
	* On Direction OfFset
	* Property Name : "BacklashZ"
	* Prop value : 1 - 5000 steps
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &ZStage::onBacklashZ);
	ret = CreateProperty(g_OnBacklashZ, "0", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minBacklashZ = 0;
	int maxBacklashZ = 50000;
	SetPropertyLimits(g_OnBacklashZ, minBacklashZ, maxBacklashZ);

    /**********************************************************************************************
    * On zTrigger
    * Property Name : "ZTrigger"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onZTrigger);
    ret = CreateProperty(g_OnZTrigger, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZTrigger = 0;
    int maxZTrigger = 10000;
    SetPropertyLimits(g_OnZTrigger, minZTrigger, maxZTrigger);

    /**********************************************************************************************
    * On xyTriggerPostDelay
    * Property Name : "XYTriggerPostDelay"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onTriggerXYPostDelay);
    ret = CreateProperty(g_OnXYTriggerPostDelayZ, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minXYTriggerPostDelayZ = 0;
    int maxXYTriggerPostDelayZ = 2000;
    SetPropertyLimits(g_OnXYTriggerPostDelayZ, minXYTriggerPostDelayZ, maxXYTriggerPostDelayZ);

    /**********************************************************************************************
    * On SetPulseDurationZ
    * Property Name : "SetPulseDurationZ"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetPulseDurationZ);
    ret = CreateProperty(g_OnSetPulseDurationZ, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minSetPulseDurationZ = 0;
    int maxSetPulseDurationZ = 10000;
    SetPropertyLimits(g_OnSetPulseDurationZ, minSetPulseDurationZ, maxSetPulseDurationZ);

    /**********************************************************************************************
    * On SetPulseDurationY
    * Property Name : "SetPulseDurationY"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetPulseDurationY);
    ret = CreateProperty(g_OnSetPulseDurationY, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minSetPulseDurationY = 0;
    int maxSetPulseDurationY = 10000;
    SetPropertyLimits(g_OnSetPulseDurationY, minSetPulseDurationY, maxSetPulseDurationY);

    /**********************************************************************************************
    * On SetPulseDurationX
    * Property Name : "SetPulseDurationX"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetPulseDurationX);
    ret = CreateProperty(g_OnSetPulseDurationX, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minSetPulseDurationX = 0;
    int maxSetPulseDurationX = 10000;
    SetPropertyLimits(g_OnSetPulseDurationX, minSetPulseDurationX, maxSetPulseDurationX);

    /**********************************************************************************************
    * On xyTriggerPreDelay
    * Property Name : "XYTriggerPreDelay"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onTriggerXYPreDelay);
    ret = CreateProperty(g_OnXYTriggerPreDelayZ, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minXYTriggerPreDelayZ = 0;
    int maxXYTriggerPreDelayZ = 2000;
    SetPropertyLimits(g_OnXYTriggerPreDelayZ, minXYTriggerPreDelayZ, maxXYTriggerPreDelayZ);


    /**********************************************************************************************
    * On zTriggerPostDelay
    * Property Name : "ZTriggerPostDelay"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onTriggerZPostDelay);
    ret = CreateProperty(g_OnZTriggerPostDelayZ, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZTriggerPostDelayZ = 0;
    int maxZTriggerPostDelayZ = 2000;
    SetPropertyLimits(g_OnZTriggerPostDelayZ, minZTriggerPostDelayZ, maxZTriggerPostDelayZ);


    /**********************************************************************************************
    * On zTriggerPostDelay
    * Property Name : "ZTriggerPostDelay"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onTriggerZPreDelay);
    ret = CreateProperty(g_OnZTriggerPreDelayZ, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZTriggerPreDelayZ = 0;
    int maxZTriggerPreDelayZ = 2000;
    SetPropertyLimits(g_OnZTriggerPreDelayZ, minZTriggerPreDelayZ, maxZTriggerPreDelayZ);

    /**********************************************************************************************
    * On zStackZSetInstance
    * Property Name : "ZStackSetInstance"
    * Prop value : 0 - 100 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onZStackSetInstance);
    ret = CreateProperty(g_OnZStackSetInstance, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZStackSetInstance = 0;
    int maxZStackSetInstance = 2000;
    SetPropertyLimits(g_OnZStackSetInstance, minZStackSetInstance, maxZStackSetInstance);

    /**********************************************************************************************
    * On EnableTrigger
    * Property Name : "EnableTrigger"
    * Prop value : ON, OFF
    * Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onEnableTrigger);
    ret = CreateProperty(g_OnEnableTrigger, "OFF", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> EnableTrigValues;
    EnableTrigValues.push_back("ON");
    EnableTrigValues.push_back("OFF");

    ret = SetAllowedValues(g_OnEnableTrigger, EnableTrigValues);

    /**********************************************************************************************
    * On zStacksYSteps
    * Property Name : "ZStackYSteps"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onZStackYSteps);
    ret = CreateProperty(g_OnZStackYSteps, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZStackYSteps = 0;
    int maxZStackYSteps = 10000;
    SetPropertyLimits(g_OnZStackYSteps, minZStackYSteps, maxZStackYSteps);
	
	/**********************************************************************************************
    * On zStacksXSteps
    * Property Name : "ZStackXSteps"
    * Prop value : 0 - 10,000 in steps
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onZStackXSteps);
    ret = CreateProperty(g_OnZStackXSteps, "1", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZStackXSteps = 0;
    int maxZStackXSteps = 10000;
    SetPropertyLimits(g_OnZStackXSteps, minZStackXSteps, maxZStackXSteps);

    /**********************************************************************************************
    * On SetZStack
    * Property Name : "SetZStack"
    * Prop value : any String input , eg(12.23,14.23,98.45) all in um
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetZStackWithDelay);
    ret = CreateProperty(g_OnSetZStack, "0,0,0,", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    ClearAllowedValues(g_OnSetZStack);

    /**********************************************************************************************
    * On SetZStackSize
    * Property Name : "SetZStackSize"
    * Prop value : 0 - 10 images in Z-Stack
    * Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetZStackSize);
    ret = CreateProperty(g_OnSetZStackSize, "0", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minZStackSize = 0;
    int maxZStackSize = 25;
    SetPropertyLimits(g_OnSetZStackSize, minZStackSize, maxZStackSize);

    /**********************************************************************************************
    * On SetZStackStepSize
    * Property Name : "SetZStackStepSize"
    * Prop value : 0 - 10 microns.
    * Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetZStackStepSize);
    ret = CreateProperty(g_OnSetZStackStepSize, "0", MM::Float, false, pAct);
    assert(ret == DEVICE_OK);

    int minZStackStepSize = 0;
    int maxZStackStepSize = 10;
    SetPropertyLimits(g_OnSetZStackStepSize, minZStackStepSize, maxZStackStepSize);

    /**********************************************************************************************
    * On SetZFgBgMap
    * Property Name : "SetZFgBgMap"
    * Prop value : any String input , eg(0,0,1,0), 0 to capture a stack and 1 for not capturing.
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &ZStage::onSetZFgBgMap);
    ret = CreateProperty(g_OnSetZFgBgMap, "0,", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    ClearAllowedValues(g_OnSetZFgBgMap);

	initialized_ = true;

	return DEVICE_OK;
}

int ZStage::Shutdown()
{
	initialized_ = false;
	return DEVICE_OK;
}

int ZStage::SetPositionUm(double posZ_um)
{
	if (restart_ || JoyStick::JoyStickEnabled_ == true) // Do this check only For the first time.
	{
		double zPos;
		GetPositionUm(zPos); // Get the current after FreshStart

		if (zPos == 0)
		{
			std::cout << "Controller Restarted" << std::endl;
			//Home();
			//std::cout << "Home Command Executed.. " << std::endl;
		}

		curZSteps_ = static_cast<long int>(zPos / getMicroStepsInUmZ_()); // convert it to steps
		std::cout << "Starting Pos: " << curZSteps_ << std::endl;
		restart_ = false;
		JoyStick::JoyStickEnabled_ = false;
	}

	microStepsInUmZ_ = getMicroStepsInUmZ_();
	//std::cout << "MicroStep Convertion val : " << microStepsInUmZ_ << std::endl;
	// Z axis movement implemented with auto Z direction  
	long int positionZ = static_cast<long>(posZ_um / microStepsInUmZ_); // Convert um to steps 

	//std::cout << "Steps Entered : " << positionZ << std::endl;
	long int z_steps = (std::abs(positionZ) - curZSteps_); // New Position to move in steps

	//std::cout << "Steps Calculated : " << z_steps << std::endl;

	if (z_steps <= 0)
		zDirection_ = 0; // Negative number leads to move towards Home
	else
		zDirection_ = 1; // Move Away home if Positive 

	curZSteps_ = std::abs(positionZ);
	if (curZSteps_ > XYZCom::limitZ_)
	{
		curZSteps_ = XYZCom::limitZ_;
		std::cout << "Upper Threshold hit in Z, Please change the limit." << std::endl;
	}

	//if (curZSteps_ < -1)
	//{
		//curZSteps_ = 0;
		//std::cout << "Negative Value hit" << std::endl;
	//}

	int ret;
	char buff[100];
	std::string respose;
	
	snprintf(buff, sizeof(buff), "$170,18,%d,%d,%ld,%d,", int(curSpeedZ_), int(curSpeedZ_), std::abs(z_steps), int(zDirection_));
	std::string SetZstr = buff;
	//std::cout << "String to Controller : " << SetZstr << std::endl;

	std::string SetZSerial = g_hub_obj.CrcCalculator(SetZstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetZSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onTriggerXYPostDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached onTriggerXYPreDelay"<<std::endl;
        long val;
        prop->Get(val);

        XYtriggerPostDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = XYtriggerPostDelay_;
        prop->Set(val);
    }
//    std::cout<<"set onTriggerXYPreDelay"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  ZTrigger pulse duration values in setps, pulse is created every Xsteps
* Values :  0  - 10000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onSetPulseDurationZ(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached SetPulseDurationZ_"<<std::endl;
        long val;
        prop->Get(val);

        SetPulseDurationZ_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = SetPulseDurationZ_;
        prop->Set(val);
    }
//    std::cout<<"set SetPulseDurationZ_"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger pulse duration values in setps, pulse is created every Xsteps
* Values :  0  - 10000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onSetPulseDurationX(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached SetPulseDurationX_"<<std::endl;
        long val;
        prop->Get(val);

        SetPulseDurationX_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = SetPulseDurationX_;
        prop->Set(val);
    }
//    std::cout<<"set SetPulseDurationX_"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  YTrigger pulse duration values in setps, pulse is created every Xsteps
* Values :  0  - 10000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onSetPulseDurationY(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached SetPulseDurationY_"<<std::endl;
        long val;
        prop->Get(val);

        SetPulseDurationY_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = SetPulseDurationY_;
        prop->Set(val);
    }
//    std::cout<<"set SetPulseDurationY_"<<std::endl;
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onTriggerXYPreDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached onTriggerXYPreDelay"<<std::endl;
        long val;
        prop->Get(val);

        XYtriggerPreDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = XYtriggerPreDelay_;
        prop->Set(val);
    }
//    std::cout<<"set onTriggerXYPreDelay"<<std::endl;
    return DEVICE_OK;
}


/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onTriggerZPostDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached onTriggerZPostDelay"<<std::endl;
        long val;
        prop->Get(val);

        ZtriggerPostDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = ZtriggerPostDelay_;
        prop->Set(val);
    }
//    std::cout<<"set onTriggerZPostDelay: "<<ZtriggerPostDelay_<<std::endl;
    return DEVICE_OK;
}


/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onTriggerZPreDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
//        std::cout<<"reached onTriggerZPreDelay"<<std::endl;
        long val;
        prop->Get(val);

        ZtriggerPreDelay_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = ZtriggerPreDelay_;
        prop->Set(val);
    }
//    std::cout<<"set onTriggerZPreDelay"<<std::endl;
    return DEVICE_OK;
}


/******************************************************************************
* Sets the  XTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 2000 1/seconds
* Default Value : 0
* Action Property
*/
int ZStage::onZStackSetInstance(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);

        ZStackSetInstance_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = ZStackSetInstance_;
        prop->Set(val);
    }
    return DEVICE_OK;
}


int ZStage::SetRelativePositionUm(double pos)
{
	int ret;
	char buff[100];
	std::string respose;

	// Implemented with auto Z Direction
	microStepsInUmZ_ = getMicroStepsInUmZ_();
	long int positionZ = static_cast<long>(pos / microStepsInUmZ_); // Convert um to steps 

	long int z_steps = positionZ;
	if (z_steps <= 0)
		zDirection_ = 0; // Negative number leads to move towards Home
	else
		zDirection_ = 1; // Move Away home if Positive 

	// Updated command Check 2.0
	curZSteps_ = curZSteps_ + positionZ; // Keep a track of absolute position(Not Relative).
	if (curZSteps_ > XYZCom::limitZ_)
	{
		curSpeedZ_ = XYZCom::limitZ_;
		std::cout << "Upper Threshold hit in Z, Please change the limit." << std::endl;
	}

	snprintf(buff, sizeof(buff), "$170,18,%d,%d,%ld,%d,", int(curSpeedZ_), int(curSpeedZ_), std::abs(static_cast<long int>(z_steps)), int(zDirection_));
	std::string SetZstr = buff;
	std::string SetZSerial = g_hub_obj.CrcCalculator(SetZstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetZSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int ZStage::GetPositionUm(double& posZ_um)
{
	try {

		int ret;
		std::string response;
		long z_steps;

		// Gets Response for Z Axiz
		std::string GetZstr = "$170,7,";      // Get comamand
		std::string GetZSerial = g_hub_obj.CrcCalculator(GetZstr); // pass the command to CrcCal
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), GetZSerial, response); // send to hub
		if (ret != DEVICE_OK) // check if the transfer of info was correct 
			return ret;

		std::stringstream ss(response);
		std::vector<std::string> result;
		std::string substr;

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			result.push_back(substr);
		}

		// Always expecting 8 string from the controller, 
		if (result.size() < 8)
		{
			return DEVICE_ERR;
		}

		/*std::cout << "Response :" << response;
		std::cout << "\tSize of vec : " << result.size() << std::endl;*/
		z_steps = std::atoi(result[5].c_str());

		// Get the convertion ratio
		microStepsInUmZ_ = getMicroStepsInUmZ_();

		posZ_um = z_steps * microStepsInUmZ_;

		return DEVICE_OK;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception from GetZ: " << e.what() << std::endl;

		return DEVICE_ERR;
	}

}

double ZStage::GetStepSize()
{
	return microStepsInUmZ_; // in um
}

int ZStage::SetPositionSteps(long steps)
{
	try {

		int ret;
		char buff[100];
		std::string respose;

		// Implemented with auto Z Direction
		long int z_steps = (std::abs(steps) - curZSteps_);

		if (z_steps <= 0)
			zDirection_ = 0; // Negative number leads to move towards Home
		else
			zDirection_ = 1; // Move Away home if Positive 

		curZSteps_ = std::abs(z_steps); // Store current steps

		snprintf(buff, sizeof(buff), "$170,18,%d,%d,%ld,%d,", int(curSpeedZ_), int(curSpeedZ_), static_cast<long int>(curZSteps_), int(zDirection_));
		std::string SetZstr = buff;
		std::string SetZSerial = g_hub_obj.CrcCalculator(SetZstr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetZSerial, respose);
		if (ret != DEVICE_OK)
			return ret;

		return DEVICE_OK;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception from SetPositionsteps : " << e.what() << std::endl;

		return DEVICE_ERR;
	}
}

int ZStage::SetRelativePositionSteps(long steps)
{
	int ret;
	char buff[100];
	std::string respose;

	// Implemented with auto Z Direction
	long int z_steps = steps;
	if (z_steps <= 0)
		zDirection_ = 0; // Negative number leads to move towards Home
	else
		zDirection_ = 1; // Move Away home if Positive 

	curZSteps_ = std::abs(z_steps); // Store current steps

	snprintf(buff, sizeof(buff), "$170,18,%d,%d,%ld,%d,", int(curSpeedZ_), int(curSpeedZ_), static_cast<long int>(curZSteps_), int(zDirection_));
	std::string SetZstr = buff;
	std::string SetZSerial = g_hub_obj.CrcCalculator(SetZstr);
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetZSerial, respose);
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int ZStage::GetPositionSteps(long& steps)
{
	try
	{
		int ret;
		std::string response;
		long z_steps;

		// Gets Response for Z Axiz
		std::string GetZstr = "$170,7,";      // Get comamand
		std::string GetZSerial = g_hub_obj.CrcCalculator(GetZstr); // pass the command to CrcCal
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), GetZSerial, response); // send to hub
		if (ret != DEVICE_OK) // check if the transfer of info was correct 
			return ret;

		std::stringstream ss(response);
		std::vector<std::string> result;
		std::string substr;

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			result.push_back(substr);
		}

		z_steps = std::atoi(result[5].c_str());

		steps = z_steps;

		return DEVICE_OK;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception from GetPositionsteps : " << e.what() << std::endl;

		return DEVICE_ERR;
	}

}

int ZStage::SetOrigin()
{
	return DEVICE_OK;
}

int ZStage::SetLimits(double lowUm, double upUm)
{
	return DEVICE_OK;
}

int ZStage::GetLimits(double& lowUm, double& upUm)
{
	return DEVICE_OK;
}

int ZStage::Home()
{
	int ret;
	std::string response;
	std::vector<std::string> getHomeZ;
	std::string substr;
	int idx = 0;
	long homeZPos;

	// Go home at Max speed
	long speedZHome = XYZCom::microStepZ_ * 500;
	if (speedZHome > 27000)
		speedZHome = 27000;
	curZSteps_ = 0;
	
	char buffZ[100];

	// Sets the Limit for XYZ Stage , always do this at home 
	snprintf(buffZ, sizeof(buffZ), "$170,4,%ld,%ld,0,0,0,1,", speedZHome, speedZHome);

	std::string HomeStr = buffZ; // Z motor are moved to Home postion with Max speed 

	std::cout << "Home SPeed Check Z : " << HomeStr << std::endl;
	std::string HomeSerial = g_hub_obj.CrcCalculator(HomeStr);

	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), HomeSerial, response);
	if (ret != DEVICE_OK)
		return ret;

	homeCounter_++; // internal check for Home cmd

	// Blocking call, waits till it reaches the stage. Max wait 
	while (idx < 900)
	{
		idx++;
		std::string getHomeZStr = "$170,17,";
		std::string getHomeZSerial = g_hub_obj.CrcCalculator(getHomeZStr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getHomeZSerial, response);
		if (ret != DEVICE_OK)
			return ret;

		std::stringstream ss(response);

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			getHomeZ.push_back(substr);
		}
		if (getHomeZ.size() < 8)
		{
			std::cout << "Vector Size Mismatch From HomeZ " << std::endl;
			std::cout << "Size : " << getHomeZ.size() << std::endl;
			return DEVICE_ERR;
		}

		homeZPos = std::atoi(getHomeZ[5].c_str());

		if (homeZPos != 0)
		{
			sleepcp(30);
			getHomeZ.clear();
		}
		else
			break;
	}

	return DEVICE_OK;
}

int ZStage::Move(double speed)
{
	return DEVICE_OK;
}

int ZStage::IsStageSequenceable(bool& isSequenceable) const
{
	isSequenceable = true;
	return DEVICE_OK;
}

bool ZStage::IsContinuousFocusDrive() const
{
	return true;
}

double ZStage::getMicroStepsInUmZ_()
{
	// code written course teeth R-P arch

//	double retVal = 1.00 / (XYZCom::microStepZ_ );
        double screwRodUm = 1000; // Inum

        double retVal = double(screwRodUm / (XYZCom::microStepZ_ * 200));

	return retVal;
}

int ZStage::ClearStageSequence()
{
    zStack_.clear();

    return DEVICE_OK;
}

int ZStage::AddToStageSequence(double position)
{
    zStack_.push_back(position);

    return DEVICE_OK;
}

int ZStage::SendStageSequence()
{
    std::vector<long int> zStackSteps;
    std::vector<long int> zStackRelative;
    long int relativeSteps = 0;
    long int negativeSteps = 0;
   // convert microns to step for the whole stack
   
    for (int idx = 0; idx < zStack_.size(); idx++)
    {
        long int zSteps = ceil(zStack_[idx] / getMicroStepsInUmZ_());
        zStackSteps.push_back(zSteps);

        // update the first element
        // First element
        if (idx == 0)
        {
            relativeSteps = zStackSteps[0] - curZSteps_;
            if (relativeSteps >= 0)
            {
                zStackRelative.push_back(relativeSteps);
            }
            else
            {
                negativeSteps = std::abs(relativeSteps);
                zStackRelative.push_back(1000 + negativeSteps);
            }
            continue;
        }

        // update the rest
        relativeSteps = zStackSteps[idx] - zStackSteps[idx - 1];
        if (relativeSteps >= 0)
        {
            zStackRelative.push_back(relativeSteps);
        }
        else
        {
            negativeSteps = std::abs(relativeSteps);
            zStackRelative.push_back(1000 + negativeSteps);
        }
        
    }

    
    std::string setZStackStr;
    std::string response;
    char buff[1000];

    snprintf(buff, sizeof(buff), "$170,27,0,%d,%zu,", zStackYSteps_, zStackRelative.size());
    setZStackStr = buff;

    std::stringstream ss;
    std::copy(zStackRelative.begin(), zStackRelative.end(), std::ostream_iterator<long int>(ss, ","));
    setZStackStr += ss.str();

//    std::cout << "SetZTrigger : " << setZStackStr << std::endl;

    std::string setZStackSerial = g_hub_obj.CrcCalculator(setZStackStr);
    
    int ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), setZStackSerial, response);
    if (ret != DEVICE_OK)
        return ret;

    return DEVICE_OK;
}


/******************************************************************************
* Sets the Z-Stack size for Z-Trigger capture during continuous acquisition.
* Values :  No. of images in Z-Stack.
* Default Value : 0
* Action Property
*/
int ZStage::onSetZStackSize(MM::PropertyBase* prop, MM::ActionType eact)
{
  if(eact == MM::AfterSet)
  {
    long val;
    prop->Get(val);
    zStackSize_ = static_cast <long> (val);
  }
  else if(eact == MM::BeforeGet)
  {
    long val;
    val = zStackSize_;
    prop->Set(val);
  }

  return DEVICE_OK;
}

/******************************************************************************
* Sets the step size for the Z-Stack for Z-Trigger capture during continuous
* acquisition.
* Values :  Step size in microns.
* Default Value : 0
* Action Property
*/
int ZStage::onSetZStackStepSize(MM::PropertyBase* prop, MM::ActionType eact)
{
  if(eact == MM::AfterSet)
  {
    double val;
    prop->Get(val);
    zStackStepSize_ = static_cast <double> (val);
  }
  else if(eact == MM::BeforeGet)
  {
    double val;
    val = zStackStepSize_;
    prop->Set(val);
  }

  return DEVICE_OK;
}

int ZStage::onSetZFgBgMap(MM::PropertyBase *prop, MM::ActionType eact)
{
  if (eact == MM::AfterSet)
  {
    prop->Get(zFgBgMap_);
  }
  else if (eact == MM::BeforeGet)
  {
    prop->Set(zFgBgMap_.c_str());
  }
  return DEVICE_OK;
}

/******************************************************************************
* Sets the ZStack, Converts string Vector to double (um) to long int (steps)
* Should be incremented
* Values :  12.45,13.45,16.37 all in um
* Default Value : "0,0,0,0"
* Action Property
*/
int ZStage::onSetZStackWithDelay(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        restart_ = true;  // Read Zval from Controller after every time this \
                                                 command is run.
        int ret;
        std::string val;
        prop->Get(val);

        // create a vector of string  from string passed
        std::stringstream ssToVec(val);
        std::vector<std::string> stringVec;
        std::string substr;
//        std::cout<<"colZStack:"<<"entered"<<std::endl;

        while (ssToVec.good())
        {
            getline(ssToVec, substr, ';'); // substring with ',' and store all the data in vector
            stringVec.push_back(substr);
//            std::cout<<"stringPassed: "<<substr<<std::endl;
        }


//        std::cout << "String Vector Size : " << stringVec.size() << std::endl;

        // convert string vector to double vector
        std::vector<double> dVec(stringVec.size());
        std::transform(stringVec.begin(), stringVec.end(), dVec.begin(), [](const std::string& Tval)
        {
            return std::stod(Tval);
        });

        // create a vector of string  from string passed
        std::stringstream ssToVec_FgBg(zFgBgMap_);
        std::vector<std::string> stringVec_FgBg;
        std::string substr_FgBg;
        while (ssToVec_FgBg.good())
        {
            getline(ssToVec_FgBg, substr_FgBg, ';'); // substring with ',' and store all the data in vector
            stringVec_FgBg.push_back(substr_FgBg);
        }

        // convert string vector to double vector
        std::vector <long> lVec_FgBg(stringVec_FgBg.size());
        std::transform(stringVec_FgBg.begin(), stringVec_FgBg.end(), lVec_FgBg.begin(), [](const std::string& Tval)
        {
            return std::stol(Tval);
        });

        // Part 2
//        std::vector<long int> zStackSteps;
        std::vector<long int> zStackRelative;
        long int relativeSteps = 0;
        long int negativeSteps = 0;

        double prevZ_Mic = 0.0;
        double prevZ_UL_Mic = 0.0;
        long prevZ_UL_Steps = curZSteps_;

        double currZ_Mic = 0.0;
        double currZ_LL_Mic = 0.0;
        long currZ_LL_Steps = 0;

        // convert microns to step for the whole stack
        for(int idx = 0; idx < dVec.size(); idx++)
        {
          if(idx > 0)
          {
            prevZ_Mic = dVec.at(idx - 1);
            if(lVec_FgBg.at(idx - 1) == 0)
            {
              prevZ_UL_Mic = prevZ_Mic + ((zStackSize_/2) * zStackStepSize_);
            }
            else
            {
              prevZ_UL_Mic = prevZ_Mic;
            }
            prevZ_UL_Steps = std::floor(prevZ_UL_Mic / getMicroStepsInUmZ_());
          }

          currZ_Mic = dVec.at(idx);
          if(lVec_FgBg.at(idx) == 0)
          {
            currZ_LL_Mic = currZ_Mic - ((zStackSize_/2 + 1) * zStackStepSize_);
          }
          else
          {
            currZ_LL_Mic = currZ_Mic;
          }
          currZ_LL_Steps = std::floor(currZ_LL_Mic / getMicroStepsInUmZ_());

          relativeSteps = currZ_LL_Steps - prevZ_UL_Steps;
          if (relativeSteps >= 0)
          {
              zStackRelative.push_back(relativeSteps);
          }
          else
          {
              negativeSteps = std::abs(relativeSteps);
              zStackRelative.push_back(1000 + negativeSteps);
          }
        }
#if 0
        for (int idx = 0; idx < dVec.size(); idx++)
        {
            long int zSteps = std::floor(dVec[idx] / getMicroStepsInUmZ_());
            zStackSteps.push_back(zSteps);
//            std::cout<<"uStep:"<<getMicroStepsInUmZ_()<<std::endl;
//            std::cout<<"dVec(um):"<<dVec[idx]<<", dVec(steps): "<<zSteps<<", currZSteps: "<<curZSteps_<<std::endl;
            // update the first element
            // First element
            if (idx == 0)
            {
                relativeSteps = zStackSteps[0] - curZSteps_;
                if (relativeSteps >= 0)
                {
                    zStackRelative.push_back(relativeSteps);
                }
                else
                {
                    negativeSteps = std::abs(relativeSteps);
                    zStackRelative.push_back(1000 + negativeSteps);
                }
                continue;
            }

            // update the rest
            relativeSteps = zStackSteps[idx] - zStackSteps[idx - 1];
            if (relativeSteps >= 0)
            {
                zStackRelative.push_back(relativeSteps);
            }
            else
            {
                long int negativeSteps = std::abs(relativeSteps);
                zStackRelative.push_back(1000 + negativeSteps);
            }
        }
#endif
//        std::cout<<"ZStackRelaive:"<<std::endl;
//        for(auto rz:zStackRelative)
//        {
//            std::cout<<rz<<"\t";
//        }
//        std::cout<<std::endl;
        std::string setZStackStr;
        std::string response;
        std::ostringstream zStackSetInstanceStr;
        char buff[1000];
        snprintf(buff, sizeof(buff), "$170,28,%d,%d,%zu,", zStackXSteps_,zStackYSteps_, zStackRelative.size());
        setZStackStr = buff;
        zStackSetInstanceStr << ZStackSetInstance_;

        std::stringstream ss;
        std::copy(zStackRelative.begin(), zStackRelative.end(), std::ostream_iterator<long int>(ss, ","));
        setZStackStr += ss.str();
        setZStackStr += zStackSetInstanceStr.str();
//       setZStackStr += "40";
        setZStackStr += ",";

//        std::cout << "SetZTrigger : " << setZStackStr << std::endl;

        std::string setZStackSerial = g_hub_obj.CrcCalculator(setZStackStr);
        setZStackSerial1=setZStackSerial;
//        std::cout<<"ColzStack:"<<setZStackSerial<std::endl;
        ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), setZStackSerial, response);

        if (ret != DEVICE_OK)
            return ret;

        return DEVICE_OK;
    }
    else if (eact == MM::BeforeGet)
    {
        std::string dummy = "0,0,0,0";
        prop->Set("0,0,0,0");
    }
    return DEVICE_OK;
}


///////////////////////////////////////////////////////////////////////////////
// Spin Scope Action handlers Z Stage
/******************************************************************************
* Sets the Speed of the Z Stage
* Non Action Property
*/
int ZStage::onSpeedZ(MM::PropertyBase* prop, MM::ActionType eact)
{
	double val = 0;

	microStepsInUmZ_ = getMicroStepsInUmZ_();

	if (eact == MM::AfterSet)
	{
		prop->Get(val);

		curSpeedZ_ = static_cast<long int>(val / (microStepsInUmZ_ / 1000));
	}
	else if (eact == MM::BeforeGet)
	{
		val = (curSpeedZ_ * (microStepsInUmZ_ / 1000));

		prop->Set(val);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the Acceleration of the Z Stage
* Values : 0.0 - 10000 Steps/sec
* Default Value : 5000
* Non Action Property
*/
int ZStage::onRampZ(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		prop->Get(rampSpeedZ_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(rampSpeedZ_);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets BacklashZ value,
* Values :  0 - 5000 steps 
* Default Value : 0
* Action Property,  Can be used for removing the backlash
*/
int ZStage::onBacklashZ(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		int ret;
		long val;
		prop->Get(val);
		backlashZ_ = int(val);

		char buff[100];
		std::string response;
		snprintf(buff, sizeof(buff), "$170,16,0,0,0,0,%d,", backlashZ_);
		std::string BacklashZStr = buff;
		
		std::string BacklashZSerial = g_hub_obj.CrcCalculator(BacklashZStr);
		ret = g_hub_obj.SetSerialCommand(*this, *GetCoreCallback(), BacklashZSerial, response);
		std::cout << "BacklashZ"<< BacklashZSerial << std::endl;
		if (ret != DEVICE_OK)
			return ret;
	}

	if (eact == MM::BeforeGet)
	{
		prop->Set(long(backlashZ_));
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the Direction of Z stage
* Values :  0(Towards Home) - 1(Away From Home)
* Default Value : 1
* Non Action Property
*/
int ZStage::onDirectionZ(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		prop->Get(zDirection_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(zDirection_);
	}
	return DEVICE_OK;
}

/******************************************************************************
* Sets the  ZTrigger values in setps, pulse is created every Xsteps
* Values :  0  - 10,000 steps
* Default Value : 0
* Action Property
*/
int ZStage::onZTrigger(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);

        zTrigger_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = zTrigger_;
        prop->Set(val);
    }
    return DEVICE_OK;
}

/******************************************************************************
* Sets the  Ztrigger .If Either one moves trigger is created of
* of a small pulse width of 0.01sec(HardCoded value)
* For LabEn adapter, Switch X and Y internally
* Values :  ON, OFF
* Default Value : OFF
* Action Property
*/

int ZStage::onEnableTrigger(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        int ret;
        std::string val;
        prop->Get(val);

        if (strcmp(val.c_str(), "ON") == 0)
        {
            char buff[100];
            getEnableTrigger_ = 1;
            std::string response;
            snprintf(buff, sizeof(buff), "$170,9,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,", 0, 0, zTrigger_, SetPulseDurationX_, SetPulseDurationY_, SetPulseDurationZ_, XYtriggerPostDelay_, ZtriggerPostDelay_, XYtriggerPreDelay_, ZtriggerPreDelay_);
//            snprintf(buff, sizeof(buff), "$170,9,%d,%d,%d,100,100,100,%d,%d,%d,%d,", 0, 0, zTrigger_, 0, 0, 0, 0);
            std::string EnableTriggerStr = buff;
            std::string EnableTriggerSerial = g_hub_obj.CrcCalculator(EnableTriggerStr);
//            std::cout<<"command For Z trigger: "<<EnableTriggerSerial<<std::endl;
            //			ret = g_hub_obj.SetSerialCommand(*this, *GetCoreCallback(), EnableTriggerSerial, response);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableTriggerSerial, response);
            if (ret != DEVICE_OK)
                return ret;
        }
        else if (strcmp(val.c_str(), "OFF") == 0)
        {
            getEnableTrigger_ = 0;
            
            std::string response;
            std::string EnableLedStr = "$170,9,0,0,0,0,0,0,0,0,0,0,";
            std::string EnableLedSerial = g_hub_obj.CrcCalculator(EnableLedStr);
            //			ret = g_hub_obj.SetSerialCommand(*this, *GetCoreCallback(), EnableLedSerial, response);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableLedSerial, response);
            if (ret != DEVICE_OK)
                return ret;
        }
        else
            assert(false); // This should never happen 

        return DEVICE_OK;
    }

    if (eact == MM::BeforeGet)
    {
        if (getEnableTrigger_ == 0)
        {
            prop->Set("OFF");
        }
        else if (getEnableTrigger_ == 1)
        {
            prop->Set("ON");
        }
    }
    return DEVICE_OK;
}


/******************************************************************************
* Sets the  ZStack YSteps values in setps, After how many Y steps ZStack
* Should be incremented
* Values :  0  - 10,000 steps
* Default Value : 0
* Action Property
*/
int ZStage::onZStackYSteps(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);
		
		zStackXSteps_ = 0;
        zStackYSteps_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = zStackYSteps_;
        prop->Set(val);
    }
    return DEVICE_OK;
}
/******************************************************************************
* Sets the  ZStack XSteps values in setps, After how many X steps ZStack
* Should be incremented
* Values :  0  - 10,000 steps
* Default Value : 0
* Action Property
*/
int ZStage::onZStackXSteps(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        long val;
        prop->Get(val);
		
		zStackYSteps_ = 0;
        zStackXSteps_ = static_cast<long int>(val);
    }
    else if (eact == MM::BeforeGet)
    {
        long val;
        val = zStackXSteps_;
        prop->Set(val);
    }
    return DEVICE_OK;
}

/******************************************************************************
* Sets the ZStack, Converts string Vector to double (um) to long int (steps)
* Should be incremented
* Values :  12.45,13.45,16.37 all in um
* Default Value : "0,0,0,0"
* Action Property
*/
int ZStage::onSetZStack(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        restart_ = true;  // Read Zval from Controller after every time this \
                                                 command is run. 
        int ret;
        std::string val;
        prop->Get(val);

        // create a vector of string  from string passed 
        std::stringstream ssToVec(val);
        std::vector<std::string> stringVec;
        std::string substr;

        while (ssToVec.good())
        {
            getline(ssToVec, substr, ';'); // substring with ',' and store all the data in vector 
            stringVec.push_back(substr);
        }

//        std::cout << "String Vector Size : " << stringVec.size() << std::endl;

        // convert string vector to double vector
        std::vector<double> dVec(stringVec.size());
        std::transform(stringVec.begin(), stringVec.end(), dVec.begin(), [](const std::string& Tval)
        {
            return std::stod(Tval);
        });
        
        // Part 2 
        std::vector<long int> zStackSteps;
        std::vector<long int> zStackRelative;
        long int relativeSteps = 0;
        long int negativeSteps = 0;

        // convert microns to step for the whole stack
        for (int idx = 0; idx < dVec.size(); idx++)
        {
            long int zSteps = std::ceil(dVec[idx] / getMicroStepsInUmZ_());
            zStackSteps.push_back(zSteps);

            // update the first element
            // First element
            if (idx == 0)
            {
                relativeSteps = zStackSteps[0] - curZSteps_;
                if (relativeSteps >= 0)
                {
                    zStackRelative.push_back(relativeSteps);
                }
                else
                {
                    negativeSteps = std::abs(relativeSteps);
                    zStackRelative.push_back(1000 + negativeSteps);
                }
                continue;
            }

            // update the rest
            relativeSteps = zStackSteps[idx] - zStackSteps[idx - 1];
            if (relativeSteps >= 0)
            {
                zStackRelative.push_back(relativeSteps);
            }
            else
            {
                long int negativeSteps = std::abs(relativeSteps);
                zStackRelative.push_back(1000 + negativeSteps);
            }

        }
//        std::cout<<"ZStackRelaive:"<<std::endl;
//        for(auto rz:zStackRelative)
//        {
//            std::cout<<rz<<"\t";
//        }
//        std::cout<<std::endl;
        std::string setZStackStr;
        std::string response;
        char buff[1000];
        snprintf(buff, sizeof(buff), "$170,27,%d,%d,%zu,", zStackXSteps_,zStackYSteps_, zStackRelative.size());
        setZStackStr = buff;

        std::stringstream ss;
        std::copy(zStackRelative.begin(), zStackRelative.end(), std::ostream_iterator<long int>(ss, ","));
        setZStackStr += ss.str();

//        std::cout << "SetZTrigger : " << setZStackStr << std::endl;

        std::string setZStackSerial = g_hub_obj.CrcCalculator(setZStackStr);

        ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), setZStackSerial, response);
        if (ret != DEVICE_OK)
            return ret;

        return DEVICE_OK;
    }
    else if (eact == MM::BeforeGet)
    {
        std::string dummy = "0,0,0,0";
        prop->Set("0,0,0,0");
    }
    return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// VulanScope XYZ Comp
///////////////////////////////////////////////////////////////////////////////
XYZCom::XYZCom() :
	CGenericBase<XYZCom>(),
	initialized_(false),
	getEnableXYZ_(0),
	getEnableSetLimitXYZ_(0),
    infoCheck_(3),
	name_(g_VulanScopeXYZDrive)
{
	InitializeDefaultErrorMessages();
	SetErrorText(ERR_DEVICE_NOT_FOUND, "No XYZ-Stage Com Found in this microscope");
	SetErrorText(ERR_PORT_NOT_SET, "No Serial port found ");
}

XYZCom::~XYZCom()
{
	Shutdown();
}

bool XYZCom::Busy()
{
	return false;
}

void XYZCom::GetName(char* name) const
{
	assert(name_.length() < CDeviceUtils::GetMaxStringLength());
	CDeviceUtils::CopyLimitedString(name, name_.c_str());
}

int XYZCom::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized()) {
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}

	// Name
	ret = CreateProperty(MM::g_Keyword_Name, name_.c_str(), MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Description
	ret = CreateProperty(MM::g_Keyword_Description, g_VulanScopeXYZDrive, MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Action Handlers 
	/**********************************************************************************************
	* XY Stage Enable
	* Property Name : "EnableXYZ"
	* Prop value : 0(Enable) or 1(disable)
	* Action Property
	*/
	CPropertyAction* pAct = new CPropertyAction(this, &XYZCom::onEnableXYZ);
	ret = CreateProperty(g_OnEnableXYZ, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> enableXYZ;
	enableXYZ.push_back("ON");
	enableXYZ.push_back("OFF");

	ret = SetAllowedValues(g_OnEnableXYZ, enableXYZ);

	/**********************************************************************************************
	* X Limit
	* Property Name : "SetLimitX"
	* Prop value : 0 - 20,000 steps
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetLimitX);
	ret = CreateProperty(g_OnSetLimitX, "0.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSetLimitX = 0.0;
	float maxSetLimitX = 250000.0;
	SetPropertyLimits(g_OnSetLimitX, minSetLimitX, maxSetLimitX);

	/**********************************************************************************************
	* Y Limit
	* Property Name : "SetLimitY"
	* Prop value : 0 - 20,000 steps
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetLimitY);
	ret = CreateProperty(g_OnSetLimitY, "0.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSetLimitY = 0.0;
	float maxSetLimitY = 250000.0;
	SetPropertyLimits(g_OnSetLimitY, minSetLimitY, maxSetLimitY);

	/**********************************************************************************************
	* Z Limit
	* Property Name : "SetLimitZ"
	* Prop value : 0 - 500,000 steps
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetLimitZ);
	ret = CreateProperty(g_OnSetLimitZ, "0.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSetLimitZ = 0.0;
	float maxSetLimitZ = 900000.0;
	SetPropertyLimits(g_OnSetLimitZ, minSetLimitZ, maxSetLimitZ);

	/**********************************************************************************************
	* XYZLimit send to serial
	* Property Name : "EnableSetLimitXYZ"
	* Prop value : 0(Send to serial) - 1(Dont send)
	* Action Property
	*/
	pAct = new CPropertyAction(this, &XYZCom::onEnableSetLimitXYZ);
	ret = CreateProperty(g_OnEnableSetLimitXYZ, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> enableSetLimitXYZ;
	enableSetLimitXYZ.push_back("ON");
	enableSetLimitXYZ.push_back("OFF");

	ret = SetAllowedValues(g_OnEnableSetLimitXYZ, enableSetLimitXYZ);

	/**********************************************************************************************
	* X Micro steps Val
	* Property Name : "MicrpStepX"
    * Prop value : 1, 2, 4, 8, 16, 32, 64, 128
	* Non Action Property
	* Note : if any other values is sent, value will be set to 1
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetMicroStepX);
	ret = CreateProperty(g_OnSetMicroStepX, "4", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> setMicroStepXVal;
	setMicroStepXVal.push_back("1");
	setMicroStepXVal.push_back("2");
	setMicroStepXVal.push_back("4");
	setMicroStepXVal.push_back("8");
	setMicroStepXVal.push_back("16");
	setMicroStepXVal.push_back("32");
    setMicroStepXVal.push_back("64");
    setMicroStepXVal.push_back("128");

	SetAllowedValues(g_OnSetMicroStepX, setMicroStepXVal);

	/**********************************************************************************************
	* Y Micro steps Val
	* Property Name : "MicrpStepY"
    * Prop value : 1, 2, 4, 8, 16, 32, 64, 128
	* Non Action Property
	* Note : if any other values is sent, value will be set to 1
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetMicroStepY);
	ret = CreateProperty(g_OnSetMicroStepY, "4", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> setMicroStepYVal;
	setMicroStepYVal.push_back("1");
	setMicroStepYVal.push_back("2");
	setMicroStepYVal.push_back("4");
	setMicroStepYVal.push_back("8");
	setMicroStepYVal.push_back("16");
	setMicroStepYVal.push_back("32");
    setMicroStepXVal.push_back("64");
    setMicroStepXVal.push_back("128");

	SetAllowedValues(g_OnSetMicroStepY, setMicroStepYVal);

	/**********************************************************************************************
	* Z Micro steps Val
	* Property Name : "MicrpStepZ"
    * Prop value : 1, 2, 4, 8, 16, 32, 64, 128
	* Non Action Property
	* Note : if any other values is sent, value will be set to 1
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetMicroStepZ);
	ret = CreateProperty(g_OnSetMicroStepZ, "4", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> setMicroStepZVal;
	setMicroStepZVal.push_back("1");
	setMicroStepZVal.push_back("2");
	setMicroStepZVal.push_back("4");
	setMicroStepZVal.push_back("8");
	setMicroStepZVal.push_back("16");
	setMicroStepZVal.push_back("32");
    setMicroStepXVal.push_back("64");
    setMicroStepXVal.push_back("128");

	SetAllowedValues(g_OnSetMicroStepZ, setMicroStepZVal);

    /**********************************************************************************************
    * Turret Micro steps Val
    * Property Name : "MicrpStepTurret"
    * Prop value : 1, 2, 4, 8, 16, 32, 64, 128
    * Non Action Property
    * Note : if any other values is sent, value will be set to 1
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetMicroStepTurret);
    ret = CreateProperty(g_OnSetMicroStepTurret, "4", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> setMicroStepTurretVal;
    setMicroStepTurretVal.push_back("1");
    setMicroStepTurretVal.push_back("2");
    setMicroStepTurretVal.push_back("4");
    setMicroStepTurretVal.push_back("8");
    setMicroStepTurretVal.push_back("16");
    setMicroStepTurretVal.push_back("32");
    setMicroStepTurretVal.push_back("64");
    setMicroStepTurretVal.push_back("128");
    SetAllowedValues(g_OnSetMicroStepTurret, setMicroStepTurretVal);

    /**********************************************************************************************
    * Run Current setting for XY motor Driver
    * Property Name : "XYRunCurrent"
    * Prop value : 0 to 31
    * Non Action Property
    * Note : if any other values is sent, value will be set to 10
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetXYRunCurrent);
    ret = CreateProperty(g_XYRunCurrentSetting, "10", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    int minXYRunCurrent = 0;
    int maxXYRunCurrent = 31;

    SetPropertyLimits(g_XYRunCurrentSetting, minXYRunCurrent, maxXYRunCurrent);

    /**********************************************************************************************
    * Run Current setting for Z motor Driver
    * Property Name : "ZRunCurrent"
    * Prop value : 0 to 31
    * Non Action Property
    * Note : if any other values is sent, value will be set to 10
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetZRunCurrent);
    ret = CreateProperty(g_ZRunCurrentSetting, "10", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    int minZRunCurrent = 0;
    int maxZRunCurrent = 31;

    SetPropertyLimits(g_ZRunCurrentSetting, minZRunCurrent, maxZRunCurrent);

    /**********************************************************************************************
    * Run Current setting for Turret motor Driver
    * Property Name : "TurretRunCurrent"
    * Prop value : 0 to 31
    * Non Action Property
    * Note : if any other values is sent, value will be set to 10
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetTurretRunCurrent);
    ret = CreateProperty(g_TurretRunCurrentSetting, "10", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    int minTurretRunCurrent = 0;
    int maxTurretRunCurrent = 31;

    SetPropertyLimits(g_TurretRunCurrentSetting, minTurretRunCurrent, maxTurretRunCurrent);

    /**********************************************************************************************
    * Hold Current setting for Turret motor Driver
    * Property Name : "TurretHoldCurrent"
    * Prop value : 0 to 31
    * Non Action Property
    * Note : if any other values is sent, value will be set to 10
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetTurretHoldCurrent);
    ret = CreateProperty(g_TurretHoldCurrentSetting, "10", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    int minTurretHoldCurrent = 0;
    int maxTurretHoldCurrent = 31;

    SetPropertyLimits(g_TurretHoldCurrentSetting, minTurretHoldCurrent, maxTurretHoldCurrent);

    /**********************************************************************************************
    * Hold Current setting for XY motor Driver
    * Property Name : "XYHoldCurrent"
    * Prop value : 0 to 31
    * Non Action Property
    * Note : if any other values is sent, value will be set to 8
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetXYHoldCurrent);
    ret = CreateProperty(g_XYHoldCurrentSetting, "8", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    int minXYHoldCurrent = 0;
    int maxXYHoldCurrent = 31;

    SetPropertyLimits(g_XYHoldCurrentSetting, minXYHoldCurrent, maxXYHoldCurrent);

    /**********************************************************************************************
    * Hold Current setting for Z motor Driver
    * Property Name : "ZHoldCurrent"
    * Prop value : 0 to 31
    * Non Action Property
    * Note : if any other values is sent, value will be set to 8
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetZHoldCurrent);
    ret = CreateProperty(g_ZHoldCurrentSetting, "8", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    int minZHoldCurrent = 0;
    int maxZHoldCurrent = 31;

    SetPropertyLimits(g_ZHoldCurrentSetting, minZHoldCurrent, maxZHoldCurrent);

    /**********************************************************************************************
    * Mode change of X stage motor driver
    * Property Name : "XStageMode"
    * Prop value : 0, 1, 2, 3
    * Non Action Property
    * Note : if any other values is sent, value will be set to 1
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetModeX);
    ret = CreateProperty(g_onSetModeX, "0", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> setXStageMode;
    setXStageMode.push_back("0");
    setXStageMode.push_back("1");
    setXStageMode.push_back("2");
    setXStageMode.push_back("3");
    SetAllowedValues(g_onSetModeX, setXStageMode);

    /**********************************************************************************************
    * Mode change of Y stage motor driver
    * Property Name : "YStageMode"
    * Prop value : 0, 1, 2, 3
    * Non Action Property
    * Note : if any other values is sent, value will be set to 1
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetModeY);
    ret = CreateProperty(g_onSetModeY, "0", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> setYStageMode;
    setYStageMode.push_back("0");
    setYStageMode.push_back("1");
    setYStageMode.push_back("2");
    setYStageMode.push_back("3");
    SetAllowedValues(g_onSetModeY, setYStageMode);

    /**********************************************************************************************
    * Mode change of Z stage motor driver
    * Property Name : "ZStageMode"
    * Prop value : 0, 1, 2, 3
    * Non Action Property
    * Note : if any other values is sent, value will be set to 1
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetModeZ);
    ret = CreateProperty(g_onSetModeZ, "0", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> setZStageMode;
    setZStageMode.push_back("0");
    setZStageMode.push_back("1");
    setZStageMode.push_back("2");
    setZStageMode.push_back("3");
    SetAllowedValues(g_onSetModeZ, setZStageMode);

    /**********************************************************************************************
    * Mode change of T stage motor driver
    * Property Name : "TStageMode"
    * Prop value : 0, 1, 2, 3
    * Non Action Property
    * Note : if any other values is sent, value will be set to 1
    */
    pAct = new CPropertyAction(this, &XYZCom::onSetModeT);
    ret = CreateProperty(g_onSetModeT, "0", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> setTStageMode;
    setTStageMode.push_back("0");
    setTStageMode.push_back("1");
    setTStageMode.push_back("2");
    setTStageMode.push_back("3");
    SetAllowedValues(g_onSetModeT, setTStageMode);

/**********************************************************************************************
* Current Setting XY send to serial
* Property Name : "EnableCurretSettingXY"
* Prop value : 0(Send to serial) - 1(Dont send)
* Action Property
*/
  pAct = new CPropertyAction(this, &XYZCom::onEnableCurrentSettingXY);
  ret = CreateProperty(g_OnEnableCurrentSettingXY, "OFF", MM::String, false, pAct);
  assert(ret == DEVICE_OK);

  std::vector<std::string> enableCurrentSettingXY;
  enableCurrentSettingXY.push_back("ON");
  enableCurrentSettingXY.push_back("OFF");

  ret = SetAllowedValues(g_OnEnableCurrentSettingXY, enableCurrentSettingXY);

/**********************************************************************************************
* Current Setting Z send to serial
* Property Name : "EnableCurretSettingZ"
* Prop value : 0(Send to serial) - 1(Dont send)
* Action Property
*/
  pAct = new CPropertyAction(this, &XYZCom::onEnableCurrentSettingZ);
  ret = CreateProperty(g_OnEnableCurrentSettingZ, "OFF", MM::String, false, pAct);
  assert(ret == DEVICE_OK);

  std::vector<std::string> enableCurrentSettingZ;
  enableCurrentSettingZ.push_back("ON");
  enableCurrentSettingZ.push_back("OFF");

  ret = SetAllowedValues(g_OnEnableCurrentSettingZ, enableCurrentSettingZ);

  /**********************************************************************************************
  * Current Setting Turret send to serial
  * Property Name : "EnableCurretSettingTurret"
  * Prop value : 0(Send to serial) - 1(Dont send)
  * Action Property
  */
    pAct = new CPropertyAction(this, &XYZCom::onEnableCurrentSettingTurret);
    ret = CreateProperty(g_OnEnableCurrentSettingTurret, "OFF", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> enableCurrentSettingTurret;
    enableCurrentSettingTurret.push_back("ON");
    enableCurrentSettingTurret.push_back("OFF");

    ret = SetAllowedValues(g_OnEnableCurrentSettingTurret, enableCurrentSettingTurret);

	/**********************************************************************************************
	* MicroStepXYZ send to serial
	* Property Name : "EnableSetMicroStepXYZ"
	* Prop value : 0(Send to serial) - 1(Dont send)
	* Action Property
	*/
    pAct = new CPropertyAction(this, &XYZCom::onEnableSetMicroStepXYZT);
    ret = CreateProperty(g_OnEnableSetMicroStepXYZT, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> enableSetMicroStepXYZ;
	enableSetMicroStepXYZ.push_back("ON");
	enableSetMicroStepXYZ.push_back("OFF");

    ret = SetAllowedValues(g_OnEnableSetMicroStepXYZT, enableSetMicroStepXYZ);

	/**********************************************************************************************
	* Set FLD Filter
	* Property Name : "SetFilter"
	* Prop value : ON and OFF
	* Action Property
	*/
	pAct = new CPropertyAction(this, &XYZCom::onSetFLDFilter);
	ret = CreateProperty(g_OnSetFLDFilter, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> setFLDFilterVal;
	setFLDFilterVal.push_back("ON");
	setFLDFilterVal.push_back("OFF");

	ret = SetAllowedValues(g_OnSetFLDFilter, setFLDFilterVal);

    /**********************************************************************************************
    * Get Information
    * Property Name : "GetInfo"
    * Prop value : Version and Date
    * Action Property
    */
    pAct = new CPropertyAction(this, &XYZCom::onGetInfo);
    ret = CreateProperty(g_OnGetInfo, "Version", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> getInfoString;
    getInfoString.push_back("Version");
    getInfoString.push_back("Date");

    ret = SetAllowedValues(g_OnGetInfo, getInfoString);

	initialized_ = true;

	return DEVICE_OK;
}

int XYZCom::Shutdown()
{
	if (initialized_)
		initialized_ = false;

	return DEVICE_OK;
}

/******************************************************************************
* Used to enable XY stage
* Values :  ON , OFF
* Default Value : OFF
* 0 is enable, 1 is disable
* Action Property
*/
int XYZCom::onEnableXYZ(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		int ret;
		std::string val;
		prop->Get(val);

		if (strcmp(val.c_str(), "ON") == 0)
		{
			getEnableXYZ_ = 1;
			std::string response;
			std::string EnableStr = "$170,1,1,0,0,0,100,";
			std::string EnableSerial = g_hub_obj.CrcCalculator(EnableStr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableSerial, response);
		}
		else if (strcmp(val.c_str(), "OFF") == 0)
		{
			getEnableXYZ_ = 0;
			std::string response;
			std::string EnableStr = "$170,1,1,1,1,1,0,";
			std::string EnableSerial = g_hub_obj.CrcCalculator(EnableStr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableSerial, response);
		}
		if (ret != DEVICE_OK)
			return ret;

	}
	else if (eact == MM::BeforeGet)
	{
		if (getEnableXYZ_ == 1)
		{
			prop->Set("ON");
		}
		else if (getEnableXYZ_ == 0)
		{
			prop->Set("OFF");
		}
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set Limit for X stage
* Values :  0 - 20,000 step
* Non Action Property
*/
int XYZCom::onSetLimitX(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		long val;
		prop->Get(val);
		XYZCom::limitX_ = val;
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(XYZCom::limitX_);
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set Limit for Y stage
* Values :  0 - 20,000 steps
* Non Action Property
*/
int XYZCom::onSetLimitY(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		long val;
		prop->Get(val);
		limitY_ = val;
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(limitY_);
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set Limit for Z stage
* Values :  0 - 500,000 steps;
* Non Action Property
*/
int XYZCom::onSetLimitZ(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		long val;
		prop->Get(val);
		limitZ_ = val;
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(limitZ_);
	}

	return DEVICE_OK;
}


/******************************************************************************
* Used to Send the XYZLimit Values to serial
* Values :  0(Send to serial), 1(Dont send to serial);
* Action Property
*/
int XYZCom::onEnableSetLimitXYZ(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		std::string val;
		prop->Get(val);
		if (strcmp(val.c_str(), "ON") == 0)
		{
			int ret;
			std::string response;
			char buff[100];
			getEnableSetLimitXYZ_ = 1;

			// Sets the Limit for XYZ Stage , always do this at home 
			snprintf(buff, sizeof(buff), "$170,8,100000,%ld,%ld,%ld,", limitX_, limitY_, limitZ_);
			std::string SetXYZLimitstr = buff;
			std::string SetXYZLimitSerial = g_hub_obj.CrcCalculator(SetXYZLimitstr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXYZLimitSerial, response);
			if (ret != DEVICE_OK)
				return ret;
		}
		if (strcmp(val.c_str(), "OFF") == 0)
		{
			getEnableSetLimitXYZ_ = 0;
			// Do nothing 
		}
	}
	else if (eact == MM::BeforeGet)
	{
		if (getEnableSetLimitXYZ_ == 1)
		{
			prop->Set("ON");
		}
		else if (getEnableSetLimitXYZ_ == 0)
		{
			prop->Set("OFF");
		}
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set MicroSteps X
* Values :  1, 2, 4, 8, 16, 32 (String)
* Non Action Property
*/
int XYZCom::onSetMicroStepX(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		std::string val;
		prop->Get(val);
		
		microStepX_ = atoi(val.c_str());
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(microStepX_);
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set MicroSteps Y
* Values :  1, 2, 4, 8, 16, 32 (String)
* Non Action Property
*/
int XYZCom::onSetMicroStepY(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		std::string val;
		prop->Get(val);

		microStepY_ = atoi(val.c_str());
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(microStepY_);
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set MicroSteps Z
* Values :  1, 2, 4, 8, 16, 32 (String)
* Non Action Property
*/
int XYZCom::onSetMicroStepZ(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		std::string val;
		prop->Get(val);

		microStepZ_ = atoi(val.c_str());
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(microStepZ_);
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Set MicroSteps Turret
* Values :  1, 2, 4, 8, 16, 32 (String)
* Non Action Property
*/
int XYZCom::onSetMicroStepTurret(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        microStepTurret_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(microStepTurret_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Z motor driver Hold Current
* Values :  0 - 31 (String)
* Non Action Property
*/
int XYZCom::onSetZHoldCurrent(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        ZHoldCurrent_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(ZHoldCurrent_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Turret motor driver Hold Current
* Values :  0 - 31 (String)
* Non Action Property
*/
int XYZCom::onSetTurretHoldCurrent(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        TurretHoldCurrent_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(TurretHoldCurrent_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of XY motor driver Hold Current
* Values :  0 - 31 (String)
* Non Action Property
*/
int XYZCom::onSetXYHoldCurrent(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        XYHoldCurrent_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(XYHoldCurrent_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of XY motor driver Run Current
* Values :  0 - 31 (String)
* Non Action Property
*/
int XYZCom::onSetXYRunCurrent(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);
        XYRunCurrent_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(XYRunCurrent_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Z motor driver Run Current
* Values :  0 - 31 (String)
* Non Action Property
*/
int XYZCom::onSetZRunCurrent(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        ZRunCurrent_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(ZRunCurrent_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Turret motor driver Run Current
* Values :  0 - 31 (String)
* Non Action Property
*/
int XYZCom::onSetTurretRunCurrent(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        TurretRunCurrent_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(TurretRunCurrent_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of X motor driver
* Values :  0, 1, 2, 3 (String)
* Non Action Property
*/
int XYZCom::onSetModeX(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        modeX_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(modeX_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Y motor driver
* Values :  0, 1, 2, 3 (String)
* Non Action Property
*/
int XYZCom::onSetModeY(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        modeY_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(modeY_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Z motor driver
* Values :  0, 1, 2, 3 (String)
* Non Action Property
*/
int XYZCom::onSetModeZ(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        modeZ_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(modeZ_);
    }

    return DEVICE_OK;
}

/******************************************************************************
* Used to Set Mode of Turret motor driver
* Values :  0, 1, 2, 3 (String)
* Non Action Property
*/
int XYZCom::onSetModeT(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);

        modeT_ = atoi(val.c_str());
    }
    else if (eact == MM::BeforeGet)
    {
        prop->Set(modeT_);
    }

    return DEVICE_OK;
}


/******************************************************************************
* Used to Send Microstepping value to controller
* Values :  ON, OFF
* Action Property
*/
int XYZCom::onEnableSetMicroStepXYZT(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		std::string val;
		prop->Get(val);
		if (strcmp(val.c_str(), "ON") == 0)
		{
			int ret;
			std::string response;
			char buff[100];
            getEnableSetMicroStepXYZT_ = 1;

			// Sets the Limit for XYZ Stage , always do this at home 
            snprintf(buff, sizeof(buff), "$170,19,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,", microStepTurret_, microStepX_, microStepY_, microStepZ_, modeT_, modeX_, modeY_, modeZ_);
			std::string SetXYZMicroStepstr = buff;
			std::string SetXYZMicroStepSerial = g_hub_obj.CrcCalculator(SetXYZMicroStepstr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXYZMicroStepSerial, response);
			if (ret != DEVICE_OK)
				return ret;
		}
		if (strcmp(val.c_str(), "OFF") == 0)
		{
            getEnableSetMicroStepXYZT_ = 0;
			// Do nothing 
		}
	}
	else if (eact == MM::BeforeGet)
	{
        if (getEnableSetMicroStepXYZT_ == 1)
		{
			prop->Set("ON");
		}
        else if (getEnableSetMicroStepXYZT_ == 0)
		{
			prop->Set("OFF");
		}
	}

	return DEVICE_OK;
}

/******************************************************************************
* Used to Send XY Driver Current value to controller
* Values :  ON, OFF
* Action Property
*/
int XYZCom::onEnableCurrentSettingXY(MM::PropertyBase* prop, MM::ActionType eact)
{

  if (eact == MM::AfterSet)
  {
    std::string val;
    prop->Get(val);
    if (strcmp(val.c_str(), "ON") == 0)
    {
      int ret;
      std::string response;
      char buff[100];
            getEnableCurrentSettingXY_ = 1;

      // Sets the Limit for XYZ Stage , always do this at home
            snprintf(buff, sizeof(buff), "$170,33,%ld,%ld,", XYRunCurrent_, XYHoldCurrent_);
      std::string SetXYCurrentStr = buff;
      std::string SetXYCurrentSerial = g_hub_obj.CrcCalculator(SetXYCurrentStr);
      ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXYCurrentSerial, response);
      if (ret != DEVICE_OK)
        return ret;
    }
    if (strcmp(val.c_str(), "OFF") == 0)
    {
            getEnableCurrentSettingXY_ = 0;
      // Do nothing
    }
  }
  else if (eact == MM::BeforeGet)
  {
        if (getEnableCurrentSettingXY_ == 1)
    {
      prop->Set("ON");
    }
        else if (getEnableCurrentSettingXY_ == 0)
    {
      prop->Set("OFF");
    }
  }

  return DEVICE_OK;
}

/******************************************************************************
* Used to Send Z Driver Current value to controller
* Values :  ON, OFF
* Action Property
*/
int XYZCom::onEnableCurrentSettingZ(MM::PropertyBase* prop, MM::ActionType eact)
{

  if (eact == MM::AfterSet)
  {
    std::string val;
    prop->Get(val);
    if (strcmp(val.c_str(), "ON") == 0)
    {
      int ret;
      std::string response;
      char buff[100];
            getEnableCurrentSettingZ_ = 1;

      // Sets the Limit for XYZ Stage , always do this at home
            snprintf(buff, sizeof(buff), "$170,34,%ld,%ld,", ZRunCurrent_, ZHoldCurrent_);
      std::string SetZCurrentStr = buff;
      std::string SetZCurrentSerial = g_hub_obj.CrcCalculator(SetZCurrentStr);
      ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetZCurrentSerial, response);
      if (ret != DEVICE_OK)
        return ret;
    }
    if (strcmp(val.c_str(), "OFF") == 0)
    {
            getEnableCurrentSettingZ_ = 0;
      // Do nothing
    }
  }
  else if (eact == MM::BeforeGet)
  {
        if (getEnableCurrentSettingZ_ == 1)
    {
      prop->Set("ON");
    }
        else if (getEnableCurrentSettingZ_ == 0)
    {
      prop->Set("OFF");
    }
  }

  return DEVICE_OK;
}

/******************************************************************************
* Used to Send XY Driver Current value to controller
* Values :  ON, OFF
* Action Property
*/
int XYZCom::onEnableCurrentSettingTurret(MM::PropertyBase* prop, MM::ActionType eact)
{

  if (eact == MM::AfterSet)
  {
    std::string val;
    prop->Get(val);
    if (strcmp(val.c_str(), "ON") == 0)
    {
      int ret;
      std::string response;
      char buff[100];
            getEnableCurrentSettingTurret_ = 1;

      // Sets the Limit for XYZ Stage , always do this at home
            snprintf(buff, sizeof(buff), "$170,35,%ld,%ld,", TurretRunCurrent_, TurretHoldCurrent_);
      std::string SetTurretCurrentStr = buff;
      std::string SetTurretCurrentSerial = g_hub_obj.CrcCalculator(SetTurretCurrentStr);
      ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetTurretCurrentSerial, response);
      if (ret != DEVICE_OK)
        return ret;
    }
    if (strcmp(val.c_str(), "OFF") == 0)
    {
            getEnableCurrentSettingTurret_ = 0;
      // Do nothing
    }
  }
  else if (eact == MM::BeforeGet)
  {
        if (getEnableCurrentSettingTurret_ == 1)
    {
      prop->Set("ON");
    }
        else if (getEnableCurrentSettingTurret_ == 0)
    {
      prop->Set("OFF");
    }
  }

  return DEVICE_OK;
}

/******************************************************************************
* Used to Set FLD Filter
* Values :  ON, OFF
* Action Property
*/
int XYZCom::onSetFLDFilter(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		std::string val;
		prop->Get(val);
		if (strcmp(val.c_str(), "ON") == 0)
		{
			int ret;
			std::string response;
			char buff[100];
			getFLDFilter_ = 1;
			int onPos = 545;
			// Sets the Limit for XYZ Stage , always do this at home 
			snprintf(buff, sizeof(buff), "$170,6,%d,0,", onPos);
			std::string SetFLDFilterstr = buff;
			std::string SetFLDFilterSerial = g_hub_obj.CrcCalculator(SetFLDFilterstr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetFLDFilterSerial, response);
			if (ret != DEVICE_OK)
				return ret;
		}
		if (strcmp(val.c_str(), "OFF") == 0)
		{
			int ret;
			std::string response;
			char buff[100];
			getFLDFilter_ = 0;
			int offPos = 430;
			// Sets the Limit for XYZ Stage , always do this at home 
			snprintf(buff, sizeof(buff), "$170,6,%d,0,", offPos);
			std::string SetFLDFilterstr = buff;
			std::string SetFLDFilterSerial = g_hub_obj.CrcCalculator(SetFLDFilterstr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetFLDFilterSerial, response);
			if (ret != DEVICE_OK)
				return ret;
		}
	}
	else if (eact == MM::BeforeGet)
	{
        if (getEnableSetMicroStepXYZT_ == 1)
		{
			prop->Set("ON");
		}
        else if (getEnableSetMicroStepXYZT_ == 0)
		{
			prop->Set("OFF");
		}
	}

	return DEVICE_OK;
}


/******************************************************************************
* Used to Get Information of the board
* Values :  Version, Date
* Action Property
*/
int XYZCom::onGetInfo(MM::PropertyBase* prop, MM::ActionType eact)
{

    if (eact == MM::AfterSet)
    {
        std::string val;
        prop->Get(val);
        if (strcmp(val.c_str(), "Version") == 0)
        {
            int ret;
            std::string response;
            char buff[100];
            infoCheck_ = 0;
            snprintf(buff, sizeof(buff), "$170,30,");
            std::string getVersionStr = buff;
            std::string getVersionSerial = g_hub_obj.CrcCalculator(getVersionStr);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getVersionSerial, response);
            if (ret != DEVICE_OK)
                return ret;

            std::cout << "Get Info String " << response << std::endl;    
            std::stringstream ss(response);
            std::vector<std::string> result;
            std::string substr;
            while (ss.good())
            {
                getline(ss, substr, ','); // substring with ',' and store all the data in vector 
                result.push_back(substr);
            }
            if (result.size() < 3)
            {
                std::cout << "Vector mismatch XYCom, onGetInfo" << std::endl;
                std::cout << "size : " << result.size() << std::endl;
                return	DEVICE_ERR;
            }
            std::stringstream rr(result[2]);
            std::vector<std::string> v;
            while (rr.good())
            {
                getline(rr, substr, ' '); // substring with ',' and store all the data in vector 
                v.push_back(substr);
            }
            version_ = v[0].c_str();
            

        }
        if (strcmp(val.c_str(), "Date") == 0)
        {
            int ret;
            std::string response;
            char buff[100];
            infoCheck_ = 1;
            snprintf(buff, sizeof(buff), "$170,30,");
            std::string getDateStr = buff;
            std::string getDateSerial = g_hub_obj.CrcCalculator(getDateStr);
            ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getDateSerial, response);
            if (ret != DEVICE_OK)
                return ret;
            std::stringstream ss(response);
            std::vector<std::string> result;
            std::string substr;

            while (ss.good())
            {
                getline(ss, substr, ','); // substring with ',' and store all the data in vector 
                result.push_back(substr);
            }
            if (result.size() < 3)
            {
                std::cout << "Vector mismatch XYCom, onGetInfo" << std::endl;
                std::cout << "size : " << result.size() << std::endl;
                return	DEVICE_ERR;
            }
            std::stringstream rr(result[2]);
            std::vector<std::string> v;
            while (rr.good())
            {
                getline(rr, substr, ' '); // substring with ',' and store all the data in vector 
                v.push_back(substr);
            }
            date_ = v[1].c_str();

        }
    }
    else if (eact == MM::BeforeGet)
    {
        if (infoCheck_ == 1)
        {
            prop->Set(date_.c_str());
        }
        else if (infoCheck_ == 0)
        {
            prop->Set(version_.c_str());
        }
    }

    return DEVICE_OK;
}
///////////////////////////////////////////////////////////////////////////////
// VulanScope Lamp
///////////////////////////////////////////////////////////////////////////////

Lamp::Lamp() :
    initialized_(false),
    getLedIllumination_(0),
    getOneXLedIllumination_(0),
    ledIlluminationState_(0),
    oneXLedIlluminationState_(0),
    getLedPower_(0),
	name_(g_VulanScopeLamp),
	open_(false)
{
	InitializeDefaultErrorMessages();
	SetErrorText(ERR_DEVICE_NOT_FOUND, "No Lamp was found in this is microscope");
	SetErrorText(ERR_PORT_NOT_SET, "No Serial port found in this device");
}

Lamp::~Lamp()
{
	Shutdown();
}

bool Lamp::Busy()
{
	return false;
}

void Lamp::GetName(char* name) const
{
	CDeviceUtils::CopyLimitedString(name, name_.c_str());
}

int Lamp::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized())
	{
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}

	// Name
	ret = CreateProperty(MM::g_Keyword_Name, name_.c_str(), MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Description
	ret = CreateProperty(MM::g_Keyword_Description, g_VulanScopeXYZDrive, MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	//Action handlers
	/**********************************************************************************************
	* On Set illumination
	* Property Name : "SetLedBrightness"
	* Prop value range : 0 - 100 % 
	* Non Action Property
	*/
	CPropertyAction *pAct = new CPropertyAction(this, &Lamp::onLedIllumination);
	ret = CreateProperty(g_OnLedIllumination, "0", MM::Integer, false, pAct);
	assert(ret == DEVICE_OK);

	int minIllumination = 0;
  int maxIllumination = 160;

	SetPropertyLimits(g_OnLedIllumination, minIllumination, maxIllumination);

    /**********************************************************************************************
    * On Set ONeillumination
    * Property Name : "SetOneXLedBrightness"
    * Prop value range : 0 - 100 %
    * Non Action Property
    */
    pAct = new CPropertyAction(this, &Lamp::onOneXLedIllumination);
    ret = CreateProperty(g_OnOneXLedIllumination, "0", MM::Integer, false, pAct);
    assert(ret == DEVICE_OK);

    int minOneXIllumination = 0;
    int maxOneXIllumination = 160;

    SetPropertyLimits(g_OnOneXLedIllumination, minOneXIllumination, maxOneXIllumination);
    
    /**********************************************************************************************
    * Set LED Power
    * Property Name : "SetLedPower"
    * Prop value : ON and OFF
    * Action Property
    */
    pAct = new CPropertyAction(this, &Lamp::onSetLedPower);
    ret = CreateProperty(g_OnSetLedPower, "OFF", MM::String, false, pAct);
    assert(ret == DEVICE_OK);

    std::vector<std::string> setLedPowerVal;
    setLedPowerVal.push_back("ON");
    setLedPowerVal.push_back("OFF");

    ret = SetAllowedValues(g_OnSetLedPower, setLedPowerVal);

	initialized_ = true;

	return DEVICE_OK;
}

int Lamp::Shutdown()
{
	if (initialized_)
	{
		initialized_ = false;
	}

	return DEVICE_OK;
}

int Lamp::SetOpen(bool state)
{
	if (state)
	{

	}
	else
	{

	}

	return DEVICE_OK;
}

int Lamp::GetOpen(bool& state)
{
	int ret;
	char buff[100];
	std::string response;
	int res_len;
	int open;

	snprintf(buff, sizeof(buff), " ");// Command to be written 
	std::string str = buff;
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), str, response);
	if (ret != DEVICE_OK)
		return ret;
	res_len = response.length();

	if (res_len == 8)
	{
		std::string str = response.substr();
		open = std::stoi(str);
		if (open == 1)
		{
			state = true;
		}
		if (open == 0)
		{
			state = false;
		}
	}
	else
	{
		return DEVICE_ERR;
	}

	return DEVICE_OK;
}

int Lamp::Fire(double deltaT)
{
	return DEVICE_OK;
}

/******************************************************************************
* Used to Control the Led Brightness for MicroScope
* Values :  0 - 100 (in precentage)
* Default value - 0
* Action Property
*/
int Lamp::onLedIllumination(MM::PropertyBase* prop, MM::ActionType eact)
{
		if (eact == MM::AfterSet)
		{
			int ret;
			long val;
			prop->Get(val);
			{
				if (val == 0)
				{
					char buff[100];
					std::string response;
					getLedIllumination_ = static_cast<int>(val);

					snprintf(buff, sizeof(buff), "$170,26,0,%d,", oneXLedIlluminationState_);

                    // Remember the led Illulmination state
                    ledIlluminationState_ = 0;

					std::string LedBrightStr = buff;
					std::string LedBrightSerial = g_hub_obj.CrcCalculator(LedBrightStr);
					ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), LedBrightSerial, response);
				}
				else
				{
					char buffOn[100];
					std::string responseOn;
					
					snprintf(buffOn, sizeof(buffOn), "$170,26,1,%d,", oneXLedIlluminationState_);

                    // Remember the led Illulmination state
                    ledIlluminationState_ = 1;

					std::string LedBrightOnStr = buffOn;
					std::string LedBrightOnSerial = g_hub_obj.CrcCalculator(LedBrightOnStr);
					ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), LedBrightOnSerial, responseOn);

					char buffControl[100];
					std::string responseControl;
					getLedIllumination_ = static_cast<int>(val);

					snprintf(buffControl, sizeof(buffControl), "$170,25,%d,%d,", getLedIllumination_, getOneXLedIllumination_);

					std::string LedBrightControlStr = buffControl;
					std::string LedBrightControlSerial = g_hub_obj.CrcCalculator(LedBrightControlStr);
					ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), LedBrightControlSerial, responseControl);

				}

			}
			if (ret != DEVICE_OK)
				return ret;

		}
		else if (eact == MM::BeforeGet)
		{
			long val = 0;
			val = getLedIllumination_;
			prop->Set(val);
		}

		return DEVICE_OK;

}


/******************************************************************************
* Used to Control the Led Brightness for OneX TOp Light
* Values :  0 - 100 (in precentage)
* Default value - 0
* Action Property
*/
int Lamp::onOneXLedIllumination(MM::PropertyBase* prop, MM::ActionType eact)
{
    if (eact == MM::AfterSet)
    {
        int ret;
        long val;
        prop->Get(val);
        {
            if (val == 0)
            {
                char buff[100];
                std::string response;
                getOneXLedIllumination_ = static_cast<int>(val);

                snprintf(buff, sizeof(buff), "$170,26,%d,0,", ledIlluminationState_);

                // Remember the state of the led for one X
                oneXLedIlluminationState_ = 0;

                std::string LedBrightStr = buff;
                std::string LedBrightSerial = g_hub_obj.CrcCalculator(LedBrightStr);
                ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), LedBrightSerial, response);
            }
            else
            {
                char buffOn[100];
                std::string responseOn;

                snprintf(buffOn, sizeof(buffOn), "$170,26,%d,1,", ledIlluminationState_);

                // Remember the state of the led for one X
                oneXLedIlluminationState_ = 1;

                std::string LedBrightOnStr = buffOn;
                std::string LedBrightOnSerial = g_hub_obj.CrcCalculator(LedBrightOnStr);
                ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), LedBrightOnSerial, responseOn);

                char buffControl[100];
                std::string responseControl;
                getOneXLedIllumination_ = static_cast<int>(val);

                snprintf(buffControl, sizeof(buffControl), "$170,25,%d,%d,", getLedIllumination_, getOneXLedIllumination_);

                std::string LedBrightControlStr = buffControl;
                std::string LedBrightControlSerial = g_hub_obj.CrcCalculator(LedBrightControlStr);
                ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), LedBrightControlSerial, responseControl);

            }

        }
        if (ret != DEVICE_OK)
            return ret;

    }
    else if (eact == MM::BeforeGet)
    {
        long val = 0;
        val = getOneXLedIllumination_;
        prop->Set(val);
    }

    return DEVICE_OK;

}

int Lamp::onSetLedPower(MM::PropertyBase* prop, MM::ActionType eact)
{
  if (eact == MM::AfterSet)
  {
    std::string val;
    prop->Get(val);
    if (strcmp(val.c_str(), "ON") == 0)
    {
      int ret;
      std::string response;
      char buff[100];
      getLedPower_ = 1;
      // Set the LED power state to ON.
      snprintf(buff, sizeof(buff), "$170,26,%d,0,", getLedPower_);
      std::string SetLedPowerstr = buff;
      std::string SetLedPowerSerial = g_hub_obj.CrcCalculator(SetLedPowerstr);
      ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetLedPowerSerial, response);
//      if (ret != DEVICE_OK)
//        return ret;
    }
    if (strcmp(val.c_str(), "OFF") == 0)
    {
      int ret;
      std::string response;
      char buff[100];
      getLedPower_ = 0;
      // Set the LED power state to OFF.
      snprintf(buff, sizeof(buff), "$170,26,%d,0,", getLedPower_);
      std::string SetLedPowerstr = buff;
      std::string SetLedPowerSerial = g_hub_obj.CrcCalculator(SetLedPowerstr);
      ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetLedPowerSerial, response);
//      if (ret != DEVICE_OK)
//        return ret;
    }
  }
  else if (eact == MM::BeforeGet)
  {
    if (getLedPower_ == 1)
    {
      prop->Set("ON");
    }
    else if (getLedPower_ == 0)
    {
      prop->Set("OFF");
    }
  }
  return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
//  VulanScope Objective Lens Not Implemented Fully
///////////////////////////////////////////////////////////////////////////////

ObjLens::ObjLens() :
	initialized_(false),
	name_(g_VulanScopeObjLens),
	pos_(0),
	objDir_(0),
	objSteps_(0),
	restart_(false),
	objSpeed_(200)
{
	InitializeDefaultErrorMessages();
	SetErrorText(ERR_INVALID_POSITION, "Objective nosepiece reports an invalid position.  Is it clicked into position correctly?");
	SetErrorText(ERR_DEVICE_NOT_FOUND, "No objective nosepiece in this microscope.");
	SetErrorText(ERR_OBJECTIVE_SET_FAILED, "Failed changing objectives.  Is the Immersion mode appropriate for the new objective?");

	// create pre-initialization properties
}

ObjLens::~ObjLens()
{
	Shutdown();
}

bool ObjLens::Busy()
{
	long pos1, pos2;
	int ret;

	ret = GetObjSteps_(pos1);
	if (ret != DEVICE_OK)
		return true;

	sleepcp(100);

	ret = GetObjSteps_(pos2);
	if (ret != DEVICE_OK)
		return true;
	//std::cout << "Pos 1 : " << posZ1 << "\tPos 2 :" << posZ2 << std::endl;

	if (pos1 == pos2)
		return false;
	else
		return true;
}

int ObjLens::Home()
{
	// Turn on only Objective Motor
	SetObjMotorON_(true);

	int ret;
	std::string response;
	std::vector<std::string> getHomeObj;
	std::string substr;
	int idx = 0;
	long homeObjPos;
		
	std::string HomeStr = "$170,4,1000,1000,1,0,0,0,"; // Only M1 is sent to Home 
	std::string HomeSerial = g_hub_obj.CrcCalculator(HomeStr);

	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), HomeSerial, response);
	if (ret != DEVICE_OK)
		return ret;

	// Blocking call, waits till it reaches the stage. Max wait 5sec
	while (idx < 500)
	{
		idx++;
		std::string getHomeXYStr = "$170,17,";
		std::string getHomeXYSerial = g_hub_obj.CrcCalculator(getHomeXYStr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getHomeXYSerial, response);
		if (ret != DEVICE_OK)
			return ret;

		std::stringstream ss(response);

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			getHomeObj.push_back(substr);
		}
		if (getHomeObj.size() < 8)
		{
			std::cout << "Vector Size Mismatch From HomeXY " << std::endl;
			std::cout << "Size : " << getHomeObj.size() << std::endl;
			return DEVICE_ERR;
		}

		homeObjPos = std::atoi(getHomeObj[2].c_str());
			
		if (homeObjPos != 0)
		{
			sleepcp(30);
			getHomeObj.clear();
		}
		else
			SetObjMotorON_(false);
			break;
	}

	return DEVICE_OK;
	
}

void ObjLens::GetName(char* name) const
{
	CDeviceUtils::CopyLimitedString(name, name_.c_str());
}

int ObjLens::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized())
	{
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}
/******************************************************************************
*JoyStick Enable
* Property Name : "EnableJoyStick"
* Prop value : 0(Enable) or 1(disable)
* Action Property
*/
	CPropertyAction *pAct = new CPropertyAction(this, &ObjLens::onSetObj);
	ret = CreateProperty(g_OnSetObj, "Home", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> setObj;
	setObj.push_back("1");
	setObj.push_back("2");
	setObj.push_back("3");
	setObj.push_back("4");
	setObj.push_back("Home");

	ret = SetAllowedValues(g_OnSetObj, setObj);

	initialized_ = true;

	return DEVICE_OK;
}

int ObjLens::Shutdown()
{
	if (initialized_)
	{
		initialized_ = false;
	}

	return DEVICE_OK;
}

int ObjLens::SetPosition(long pos)
{
	return DEVICE_OK;
}

int ObjLens::SetPosition(const char* label)
{
	return DEVICE_OK;
}

int ObjLens::GetPosition(long& pos)
{
	return DEVICE_OK;
}

int ObjLens::GetPosition(const char* label)
{
	return DEVICE_OK;
}

int ObjLens::GetPositionLabel(long pos, const char* label)
{
	return DEVICE_OK;
}

int ObjLens::GetLabelPosition(const char* label, long& pos)
{
	return DEVICE_OK;
}

int ObjLens::SetPositionLabel(long pos, const char* label)
{
	return DEVICE_OK;
}

unsigned long ObjLens::GetNumberOfPositions() const
{
	return 6;
}

int ObjLens::SetGateOpen(bool open)
{
	return DEVICE_OK;
}

int ObjLens::GetGateOpen(bool& open)
{
	return DEVICE_OK;
}

int ObjLens::SetObjMotorON_(bool state)
{
	std::string response;
	int ret;
	if (state) // True, Turn ON the motor
	{
		std::string EnableStr = "$170,1,0,0,0,0,100,";
		std::string EnableSerial = g_hub_obj.CrcCalculator(EnableStr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableSerial, response);
	}
	else // Turn only the OBJ motor Off
	{
		std::string EnableStr = "$170,1,1,0,0,0,50,";
		std::string EnableSerial = g_hub_obj.CrcCalculator(EnableStr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableSerial, response);
	}
	if (ret != DEVICE_OK)
		return ret;

	return DEVICE_OK;
}

int ObjLens::GetXYZSteps_(long& xSteps, long& ySteps, long& zSteps)
{
	int ret;
	std::string response;
	
	// Gets Response for XYZ Axiz
	std::string GetObjstr = "$170,7,";      // Get comamand
	std::string GetObjSerial = g_hub_obj.CrcCalculator(GetObjstr); // pass the command to CrcCal
	ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), GetObjSerial, response); // send to hub
	if (ret != DEVICE_OK) // check if the transfer of info was correct 
		return ret;

	std::stringstream ss(response);
	std::vector<std::string> result;
	std::string substr;

	while (ss.good())
	{
		getline(ss, substr, ','); // substring with ',' and store all the data in vector 
		result.push_back(substr);
	}

	xSteps = std::atoi(result[3].c_str());
	ySteps = std::atoi(result[4].c_str());
	zSteps = std::atoi(result[5].c_str());

	
	return ret;
}

int ObjLens::GetObjSteps_(long& objSteps)
{
	try
	{
		int ret;
		std::string response;
		long obj_steps;

		// Gets Response for Z Axiz
		std::string GetObjstr = "$170,7,";      // Get comamand
		std::string GetObjSerial = g_hub_obj.CrcCalculator(GetObjstr); // pass the command to CrcCal
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), GetObjSerial, response); // send to hub
		if (ret != DEVICE_OK) // check if the transfer of info was correct 
			return ret;

		std::stringstream ss(response);
		std::vector<std::string> result;
		std::string substr;

		while (ss.good())
		{
			getline(ss, substr, ','); // substring with ',' and store all the data in vector 
			result.push_back(substr);
		}

		obj_steps = std::atoi(result[2].c_str());

		objSteps = obj_steps;

		return DEVICE_OK;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception from GetPositionsteps : " << e.what() << std::endl;

		return DEVICE_ERR;
	}
}

int ObjLens::SetObjSpeed_(int speed)
{
	objSpeed_ = speed;

	return DEVICE_OK;
}

int ObjLens::SetObjMicroStepping_(bool status)
{
	int objMicroStepping = 4;

	if (status)
	{ 
		// If true then Set MicrpStepping for Objective Movement

		int ret;
		std::string response;
		char buff[100];
	
		// Sets the Limit for XYZ Stage , always do this at home 
		snprintf(buff, sizeof(buff), "$170,19,%d,%ld,%ld,", objMicroStepping, XYZCom:: microStepY_, XYZCom::microStepZ_);
		std::string SetXYZMicroStepstr = buff;
		std::string SetXYZMicroStepSerial = g_hub_obj.CrcCalculator(SetXYZMicroStepstr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXYZMicroStepSerial, response);
	}
	else
	{
		// if False Set objective Movement
		int ret;
		std::string response;
		char buff[100];

		// Sets the Limit for XYZ Stage , always do this at home 
		snprintf(buff, sizeof(buff), "$170,19,%ld,%ld,%ld,", XYZCom::microStepX_, XYZCom::microStepY_, XYZCom::microStepZ_);
		std::string SetXYZMicroStepstr = buff;
		std::cout << "Set MicroSteping : " << SetXYZMicroStepstr << std::endl;
		std::string SetXYZMicroStepSerial = g_hub_obj.CrcCalculator(SetXYZMicroStepstr);
		ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), SetXYZMicroStepSerial, response);

	}

	return 0;
}

/******************************************************************************
* Used to Set objective
* Values :  1, 2, 3, 4
* Default Value : 1
* Action Property
*/
int ObjLens::onSetObj(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		int ret;
		std::string val;
		int lensPos = 0;
		char buff[100];
		std::string response;

		prop->Get(val);

		// Set speed once in steps / sec
        SetObjSpeed_(2000);

		//Set it to Home for the first time 
		/*if (!restart_)
		{
			SetObjMotorON_(true);
			SetObjMicroStepping_(true);

			Home();

			SetObjMicroStepping_(false);
			SetObjMotorON_(false);

			restart_ = true;
			std::cout << "Obj Home command Executed" << std::endl;
		}*/

		if (strcmp(val.c_str(), "1") == 0)
		{					
			long objPos;
			long xSteps;
			long ySteps;
			long zSteps;
			pos_ = 1;		// Objective positions 

			// Get the current objective position
			GetObjSteps_(objPos);
			std::cout << "Get Position : " << objPos << std::endl;
            objPos = (2000 - objPos);
			//std::cout << "20X objPos" << objPos << std::endl;

			// Find which directing to move 
			if ((objPos) >= 0)
				objDir_ = 1;
			else
				objDir_ = 0;

			objPos = std::abs(objPos);

			// Get XYZ stage values
			GetXYZSteps_(xSteps, ySteps, zSteps);
		
			// Turn only Objective Motor ON
			SetObjMotorON_(true);
		
//			SetObjMicroStepping_(true);
			
			snprintf(buff, sizeof(buff), "$170,2,%d,%d,%ld,%d,%d,%d,%d,0,0,0,", objSpeed_, objSpeed_, objPos, 0 , 0, 0, objDir_);
			std::string nolens = buff;
			
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), nolens, response);
			if (ret != DEVICE_OK)
			{
				return ret;
			}

			int idx = 0;
			while (idx < 500) // Max wait for 5 secs
			{
				if (Busy())
					sleepcp(10);
				else
					break;
				idx++;
			}

			// Turn only objective motor OFF
//            SetObjMicroStepping_(false);
			SetObjMotorON_(false);

		}

		if (strcmp(val.c_str(), "2") == 0)
		{
			// For 40X
			long objPos;
			long xSteps;
			long ySteps;
			long zSteps;
			pos_ = 2;
			// Get the current objective position
			GetObjSteps_(objPos);
            objPos = ((4000 ) - objPos);
			//std::cout << "40X objPos" << objPos << std::endl;

			// Find which directing to move 
			if ((objPos) >= 0)
				objDir_ = 1;
			else
				objDir_ = 0;

			objPos = std::abs(objPos);

			// Get XYZ stage values
			GetXYZSteps_(xSteps, ySteps, zSteps);

			// Turn only Objective Motor ON
			SetObjMotorON_(true);
//			SetObjMicroStepping_(true);

			snprintf(buff, sizeof(buff), "$170,2,%d,%d,%ld,%d,%d,%d,%d,0,0,0,", objSpeed_, objSpeed_, objPos, 0, 0, 0, objDir_);
			std::string nolens = buff;
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), nolens, response);
			if (ret != DEVICE_OK)
			{
				return ret;
			}

			int idx = 0;
			while (idx < 500) // Max wait for 5 secs
			{
				if (Busy())
					sleepcp(10);
				else
					break;
				idx++;
			}

			// Turn only objective motor OFF
//			SetObjMicroStepping_(false);
			SetObjMotorON_(false);

		}

		if (strcmp(val.c_str(), "3") == 0)
		{
			// For 60X
			long objPos;
			long xSteps;
			long ySteps;
			long zSteps;
			pos_ = 3;
			// Get the current objective position
			GetObjSteps_(objPos);
            objPos = ((5990) - objPos);
			//std::cout << "60X objPos" << objPos << std::endl;

			// Find which directing to move 
			if ((objPos) >= 0)
				objDir_ = 1;
			else
				objDir_ = 0;

			objPos = std::abs(objPos);

			// Get XYZ stage values
			GetXYZSteps_(xSteps, ySteps, zSteps);

			// Turn only Objective Motor ON
			SetObjMotorON_(true);
//			SetObjMicroStepping_(true);

			snprintf(buff, sizeof(buff), "$170,2,%d,%d,%ld,%d,%d,%d,%d,0,0,0,", objSpeed_, objSpeed_, objPos, 0, 0, 0, objDir_);
			std::string nolens = buff;;
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), nolens, response);
			if (ret != DEVICE_OK)
			{
				return ret;
			}

			int idx = 0;
			while (idx < 500) // Max wait for 5 secs
			{
				if (Busy())
					sleepcp(10);
				else
					break;
				idx++;
			}

			// Turn only objective motor OFF
//			SetObjMicroStepping_(false);
			SetObjMotorON_(false);

		}

		if (strcmp(val.c_str(), "4") == 0)
		{
			// For 100X
			long objPos;
			long xSteps;
			long ySteps;
			long zSteps;
			pos_ = 4;
			// Get the current objective position
			GetObjSteps_(objPos);
            objPos = ((7970) - objPos);
			//std::cout << "100X objPos" << objPos << std::endl;

			// Find which directing to move 
			if ((objPos) >= 0)
				objDir_ = 1;
			else
				objDir_ = 0;

			objPos = std::abs(objPos);

			// Get XYZ stage values
			GetXYZSteps_(xSteps, ySteps, zSteps);

			// Turn only Objective Motor ON
			SetObjMotorON_(true);
//			SetObjMicroStepping_(true);

			snprintf(buff, sizeof(buff), "$170,2,%d,%d,%ld,%d,%d,%d,%d,0,0,0,", objSpeed_, objSpeed_, objPos, 0, 0, 0, objDir_);
			std::string nolens = buff;;
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), nolens, response);
			if (ret != DEVICE_OK)
			{
				return ret;
			}

			int idx = 0;
			while (idx < 500) // Max wait for 5 secs
			{
				if (Busy())
					sleepcp(10);
				else
					break;
				idx++;
			}

			// Turn only objective motor OFF
//			SetObjMicroStepping_(false);
			SetObjMotorON_(false);
		}
		
		if (strcmp(val.c_str(), "Home") == 0)
		{

			// Turn only Objective Motor ON
			SetObjMotorON_(true);
//			SetObjMicroStepping_(true);

			int ret;
			std::string response;
			std::vector<std::string> getHomeObj;
			std::string substr;
			int idx = 0;
			long homeObjPos;

			std::string HomeStr = "$170,4,2000,2000,1,0,0,0,"; // Only M1 is sent to Home 
			std::string HomeSerial = g_hub_obj.CrcCalculator(HomeStr);

			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), HomeSerial, response);
			if (ret != DEVICE_OK)
				return ret;

			// Blocking call, waits till it reaches the stage. Max wait 5sec
			while (idx < 500)
			{
				idx++;
				std::string getHomeXYStr = "$170,17,";
				std::string getHomeXYSerial = g_hub_obj.CrcCalculator(getHomeXYStr);
				ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), getHomeXYSerial, response);
				if (ret != DEVICE_OK)
					return ret;

				std::stringstream ss(response);

				while (ss.good())
				{
					getline(ss, substr, ','); // substring with ',' and store all the data in vector 
					getHomeObj.push_back(substr);
				}
				if (getHomeObj.size() < 8)
				{
					std::cout << "Vector Size Mismatch From HomeXY " << std::endl;
					std::cout << "Size : " << getHomeObj.size() << std::endl;
					return DEVICE_ERR;
				}

				homeObjPos = std::atoi(getHomeObj[2].c_str());

				if (homeObjPos != 0)
				{
					sleepcp(30);
					getHomeObj.clear();
				}
				else
					break;
			}


			// Turn only Objective Motor ON
			SetObjMotorON_(false);
//			SetObjMicroStepping_(false);

		}

	}

	else if (eact == MM::BeforeGet)
	{
		long objSteps;
		GetObjSteps_(objSteps);
		std::cout << "GetObjSteps " << objSteps << std::endl;

		if (objSteps == 990)
		{
            prop->Set("1");
		}
		else if (objSteps == 1985)
		{
            prop->Set("2");
		}
		else if (objSteps == 2975)
		{
            prop->Set("3");
		}
		else if (objSteps == 3965)
		{
            prop->Set("4");
		}
	}

	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
//  VulanScope JoyStick 
///////////////////////////////////////////////////////////////////////////////

JoyStick::JoyStick() :
	CGenericBase<JoyStick>(),
	initialized_(false),
	getEnableJS_(0),
	speedJS_(200),
	name_(g_VulanScopeJoyStick)
{
	InitializeDefaultErrorMessages();
	SetErrorText(ERR_DEVICE_NOT_FOUND, "No JoyStick Found in this microscope");
	SetErrorText(ERR_PORT_NOT_SET, "No Serial port found ");
}

JoyStick::~JoyStick()
{
	Shutdown();
}

bool JoyStick::Busy()
{
	return false;
}

void JoyStick::GetName(char* name) const
{
	assert(name_.length() < CDeviceUtils::GetMaxStringLength());
	CDeviceUtils::CopyLimitedString(name, name_.c_str());
}

int JoyStick::Initialize()
{
	int ret;
	if (!g_hub_obj.Initialized()) {
		ret = g_hub_obj.Initialize(*this, *GetCoreCallback());
		if (ret != DEVICE_OK)
			return ret;
	}

	// Name
	ret = CreateProperty(MM::g_Keyword_Name, name_.c_str(), MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Description
	ret = CreateProperty(MM::g_Keyword_Description, g_VulanScopeJoyStick, MM::String, true);
	if (DEVICE_OK != ret)
		return ret;

	// Action Handlers 
	/**********************************************************************************************
	* JoyStick Enable
	* Property Name : "EnableJoyStick"
	* Prop value : 0(Enable) or 1(disable)
	* Action Property
	*/
	CPropertyAction* pAct = new CPropertyAction(this, &JoyStick::onEnableJS);
	ret = CreateProperty(g_OnEnableJoyStick, "OFF", MM::String, false, pAct);
	assert(ret == DEVICE_OK);

	std::vector<std::string> enableJS;
	enableJS.push_back("ON");
	enableJS.push_back("OFF");

	ret = SetAllowedValues(g_OnEnableJoyStick, enableJS);

	/**********************************************************************************************
	* On JoyStick Speed
	* Property Name : "SpeedJoyStick"
	* Prop value range : 0 - 5000 steps/ sec
	* Non Action Property
	*/
	pAct = new CPropertyAction(this, &JoyStick::onSpeedJS);
	ret = CreateProperty(g_OnSpeedJoyStick, "200.0", MM::Float, false, pAct);
	assert(ret == DEVICE_OK);

	float minSpeedJS = 0.0;
	float maxSpeedJS = 5000.0;

	SetPropertyLimits(g_OnSpeedJoyStick, minSpeedJS, maxSpeedJS);

	initialized_ = true;

	return DEVICE_OK;
}

int JoyStick::Shutdown()
{
	initialized_ = false;
	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Spin Scope Action handlers JoyStick
/******************************************************************************
* Used to enable JoyStick
* Values :  ON , OFF
* Default Value : OFF
* 0 is enable, 1 is disable
* Action Property
*/
int JoyStick::onEnableJS(MM::PropertyBase* prop, MM::ActionType eact)
{
	if (eact == MM::AfterSet)
	{
		char buff[100];
		int ret;
		std::string val;
		prop->Get(val);

		if (strcmp(val.c_str(), "ON") == 0)
		{
			getEnableJS_ = 1;
			std::string response;
			JoyStickEnabled_ = true;

			snprintf(buff, sizeof(buff), "$170,12,%ld,%ld,1,1,1,", speedJS_, speedJS_);
			std::string EnableJSStr = buff;
			std::string EnableJSSerial = g_hub_obj.CrcCalculator(EnableJSStr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), EnableJSSerial, response);
		}
		else if (strcmp(val.c_str(), "OFF") == 0)
		{
			getEnableJS_ = 0;
			std::string response;
			std::string DisableJSStr = "$170,13,0,0,0,0,0,";
			std::string DisableJSSerial = g_hub_obj.CrcCalculator(DisableJSStr);
			ret = g_hub_obj.SerialCommand(*this, *GetCoreCallback(), DisableJSSerial, response);

			// Pass the new position to SetPosition
		}
		if (ret != DEVICE_OK)
			return ret;

	}
	else if (eact == MM::BeforeGet)
	{
		if (getEnableJS_ == 1)
		{
			prop->Set("ON");
		}
		else if (getEnableJS_ == 0)
		{
			prop->Set("OFF");
		}
	}

	return DEVICE_OK;
}

/******************************************************************************
* Sets the Speed of the Joy Stick
* Values : 0.0 - 5000 steps/sec
* Default Value : 200
* Non Action Property
*/
int JoyStick::onSpeedJS(MM::PropertyBase* prop, MM::ActionType eact)
{

	if (eact == MM::AfterSet)
	{
		prop->Get(speedJS_);
	}
	else if (eact == MM::BeforeGet)
	{
		prop->Set(speedJS_);
	}
	return DEVICE_OK;
}
