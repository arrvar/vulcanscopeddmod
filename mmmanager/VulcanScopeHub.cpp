///////////////////////////////////////////////////////////////////////////////////////////////////
// FILE:          VulanScopeHub.cpp
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   VulanScopeHub module. Required for operation of VulanScope device.
//                                                                                     
// AUTHOR:        GireeshWaran, 6-November-2016
//                Based on LeicaDSMTC adapter by G. Esteban Fernandez
//				  and LabEnDMI by Manish Shiralakr
//

#define _CRT_SECURE_NO_DEPRECATE

#include "assert.h"
#include <memory.h>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include "VulcanScopeHub.h"
#include "VulcanScope.h"

#ifdef WIN32
#include <windows.h>
#endif

#ifdef linux
#include <unistd.h>
#endif

void sleepcp(int millisec); // Cross platform sleep function
void sleepcp(int millisec)
{
#ifdef WIN32		
	Sleep(millisec);
#else
	usleep(millisec * 1000);
#endif
}

VulanScopeHub::VulanScopeHub() :
	port_(""),
	version_(""),
	microscope_(""),
	initialized_(false)
{
	_clearRcvBuff();
}

VulanScopeHub::~VulanScopeHub()
{
	initialized_ = false;
}

int VulanScopeHub::Initialize(MM::Device& device, MM::Core& core)
{
	if (initialized_)
		return DEVICE_OK;
	
	initialized_ = true;

	return DEVICE_OK;
}

void VulanScopeHub::SetPort(const char* port)
{
	port_ = port;
}

int VulanScopeHub::DeInitialize()
{
	initialized_ = false;
	return DEVICE_OK;
}

bool VulanScopeHub::Initialized()
{
	return initialized_;
}

std::string VulanScopeHub::Version()
{
	return version_;
}

std::string VulanScopeHub::Microscope()
{
	return microscope_;
}

///////////////////////////////////////////////////////////////////////////////
// Device commands
///////////////////////////////////////////////////////////////////////////////

/*
 * Clears the receive buffer
 */
void VulanScopeHub::_clearAllRcvBuff(MM::Device& device, MM::Core& core)
{	
	// Read all the data received so far
	unsigned long read = RCV_BUF_LENGTH;
	while (read == (unsigned long)RCV_BUF_LENGTH)
	{
		core.ReadFromSerial(&device, port_.c_str(), (unsigned char*)rcvbuf_, RCV_BUF_LENGTH, read);
	}

	// Delete it all
	memset(rcvbuf_, 0, RCV_BUF_LENGTH);
}

void VulanScopeHub::_clearRcvBuff()
{
	memset(rcvbuf_, 0, RCV_BUF_LENGTH);
}

/*
* Sends serial command to MMCore virtual serial Port 
*/
int VulanScopeHub::SerialCommand(MM::Device& device, MM::Core& core, std::string command, std::string &response)
{
	if (port_ == "")
		return DEVICE_NOT_CONNECTED;
	
	_clearAllRcvBuff(device, core);              

//  std::cout << "Serial Cmd: " << command << std::endl;
	int ret = core.SetSerialCommand(&device, port_.c_str(), command.c_str(), "\r\n");
	if (ret != DEVICE_OK)
	{
		return ret;
	}

	// Sleep for some milliseconds before reading response from the controller
	sleepcp(10);

	unsigned long read = 100;
	char received_buf[100];

	ret = core.GetSerialAnswer(&device, port_.c_str(), 100, (char*)received_buf, "\r\n");
	if (ret != DEVICE_OK)
		return ret;
	
	std::string str(received_buf);
	//std::cout << "Rx cmd : " << received_buf << std::endl;
	response = str;
	
	return DEVICE_OK;
}

/*
* This is used for sending Set commands only, it Dosen't wait for the answer from the Serial port.
*/
int VulanScopeHub::SetSerialCommand(MM::Device& device, MM::Core& core, std::string command, std::string &response)
{
	if (port_ == "")
		return DEVICE_NOT_CONNECTED;

	_clearAllRcvBuff(device, core);

	int ret = core.SetSerialCommand(&device, port_.c_str(), command.c_str(), "\r\n");
	if (ret != DEVICE_OK)
	{
		return ret;
	}

	// Sleep required between two commands else no command is executed.
	sleepcp(10);

	//unsigned long read = 100;
	//char received_buf[100];

	// Dummy read
	//ret = core.GetSerialAnswer(&device, port_.c_str(), 100, (char*)received_buf, "\r\n");
	//if (ret != DEVICE_OK)
		//return ret;

	//std::string resp(received_buf);
	//std::cout << "Rx cmd: " << resp << std::endl;
	return DEVICE_OK;
}

/*
* Calculates crc for any input String
* Returns the string with appended crc
* After this pipline the commands can be sent to controller
*/
std::string VulanScopeHub::CrcCalculator(std::string input)
{
	// crc calclulated for the  input string 
	int crc = 1;
	char buff[100];
	//snprintf(buff, sizeof(buff), "%d,!\r\n", crc);

	snprintf(buff, sizeof(buff), "%d!\r\n", crc);
	std::string append = buff; // string to be appended before  sending it to serial.
	std::string out = input + append;

	//std::cout << "Input to Serial : " << out << std::endl;
	return out;
}
