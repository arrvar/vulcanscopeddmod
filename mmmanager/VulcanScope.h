///////////////////////////////////////////////////////////////////////////////////////////////////
// FILE:          VulanScope.h
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   VulanScope adapters
//                                                                                     
// AUTHOR:        GireeshWaran, 6-November-2016
//                Based on LeicaDSMTC adapter by G. Esteban Fernandez
//
//COPYRIGH:		  Spectral Inisights Pvt Ltd

#ifndef _VulanScope_H_
#define _VulanScope_H_

#include "../../MMDevice/DeviceBase.h"
#include <string>
#include <map>	

#define ERR_UNKNOWN_COMMAND          10002
#define ERR_UNKNOWN_POSITION         10003
#define ERR_HALT_COMMAND             10004
#define ERR_CANNOT_CHANGE_PROPERTY   10005

#define ERR_PORT_NOT_SET            11001
#define ERR_NOT_CONNECTED           11002
#define ERR_COMMAND_CANNOT_EXECUTE  11003
#define ERR_NO_ANSWER               11004
#define ERR_DEVICE_NOT_FOUND        11005
#define ERR_UNEXPECTED_ANSWER       11006
#define ERR_INDEX_OUT_OF_BOUNDS     11007
#define ERR_INVALID_REFLECTOR_TURRET 11008
#define ERR_INVALID_POSITION        11009
#define ERR_OBJECTIVE_SET_FAILED    11010

#define ERR_UNEXPECTED_ANSWER_GetCommandString	110061
#define ERR_UNEXPECTED_ANSWER_SetCommandData	110062
#define ERR_UNEXPECTED_ANSWER_SetCommandDataXY	110063
#define ERR_UNEXPECTED_ANSWER_SetCommand		110064

class Hub : public CGenericBase<Hub>
{
public:
	Hub();
	~Hub();
	
	// Device API
	int Initialize();
	int Shutdown();

	void GetName(char* name) const;
	bool Busy();

	// Action interface
	int OnPort(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string version_;
	std::string microscope_;
	std::string port_;

};

class XYStage : public CXYStageBase<XYStage>
{
public:
	XYStage();
	~XYStage();

	bool Busy();
	void GetName(char* name) const;
	int Initialize();
	int Shutdown();

	// Stage API
	int SetPositionUm(double posX, double posY);
	int SetRelativePositionUm(double posX, double posY);
	int SetAdapterOriginUm(double posX, double posY);
	int GetPositionUm(double& posX, double& posY);
	int GetLimitsUm(double& xMin, double& xMax, double& yMin, double& yMax);
	//int Move(double speedX, double speedY);
	int SetPositionSteps(long stepsX, long stepsY);
	int GetPositionSteps(long& stepsX, long& stepsY);
	int SetRelativePositionSteps(long stepsX, long stepsY);
	int Home();
    int XStageHome();
    int YStageHome();
	int Stop();
	int SetOrigin();
	//int SetXOrigin();
	//int SetYOrigin();
	int GetStepLimits(long& xMin, long& xMax, long& yMin, long& yMax);
	double GetStepSizeXUm();
	double GetStepSizeYUm();
	int IsXYStageSequenceable(bool& isSequenceable) const;
	
	// Action handlers 
    int onTriggerXYPostDelay(MM::PropertyBase* prop, MM::ActionType eact);
    int onTriggerXYPreDelay(MM::PropertyBase* prop, MM::ActionType eact);
    int onTriggerZPostDelay(MM::PropertyBase* prop, MM::ActionType eact);
    int onTriggerZPreDelay(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetPulseDurationX(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetPulseDurationY(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetPulseDurationZ(MM::PropertyBase* prop, MM::ActionType eact);
	int onSpeedXY(MM::PropertyBase* prop, MM::ActionType eact);
	int onRampXY(MM::PropertyBase* prop, MM::ActionType eact);
	int onDirectionX(MM::PropertyBase* prop, MM::ActionType eact);
	int onDirectionY(MM::PropertyBase* prop, MM::ActionType eact);
	int onXTrigger(MM::PropertyBase* prop, MM::ActionType eact);
	int onYTrigger(MM::PropertyBase* prop, MM::ActionType eact);
    int onEnableTrigger(MM::PropertyBase* prop, MM::ActionType eact);
    int onEnableCommandLineAccess(MM::PropertyBase* prop, MM::ActionType eact);
    int onEnableNewAcqTrigger(MM::PropertyBase* prop, MM::ActionType eact);
	// JoyStick command 
	int onSpeedJS(MM::PropertyBase* prop, MM::ActionType eact);
	int onEnableJS(MM::PropertyBase* prop, MM::ActionType eact);

	int onGetStepSizeXYUm(MM::PropertyBase* prop, MM::ActionType eact);

  int onSetZStackTable(MM::PropertyBase* prop, MM::ActionType eact);
  int onSendDesiredCommand(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string name_;
	double curSpeed_;
	double rampSpeed_;
	double xDirection_;
	double yDirection_;
	long curXSteps_;
	long curYSteps_;   
	long int limitXSteps_;
	long int limitYSteps_ ;
	int xTrigger_;
	int yTrigger_;
    int XYtriggerPreDelay_;
    int XYtriggerPostDelay_;
    int ZtriggerPreDelay_;
    int ZtriggerPostDelay_;
    int SetPulseDurationX_;
    int SetPulseDurationY_;
    int SetPulseDurationZ_;
	int getEnableTrigger_;
    int getEnableCommandLine_;
    int getEnableNewAcqTrigger_;
	int getHomeXY_;
	long speedJS_;
	int getEnableJS_;
	double microStepsInUmX_; 
	double microStepsInUmY_;
  std::string command_;
  std::string zStackTable_;

	double getMicroStepsInUmX_();
	double getMicroStepsInUmY_();
};

class ZStage : public CStageBase<ZStage>
{ 
public:
	ZStage();
	~ZStage();

	bool Busy();
	void GetName(char* pszName) const;
	int Initialize();
	int Shutdown();
    std::string setZStackSerial1;

	// Stage API   
	int SetPositionUm(double posZ_um);
	int SetRelativePositionUm(double pos);
	int GetPositionUm(double& posZ_um);
	double GetStepSize();
	int SetPositionSteps(long steps);
	int SetRelativePositionSteps(long steps);
	int GetPositionSteps(long& steps);
	int SetOrigin();
	int SetLimits(double lowUm, double upUm);
	int GetLimits(double& lowUm, double& upUm);
	int Home();
	int Move(double speed);
	int IsStageSequenceable(bool& isSequenceable) const;
	bool IsContinuousFocusDrive() const;
   
    int ClearStageSequence();
    int AddToStageSequence(double position);
    int SendStageSequence();
	// Action handlers 
	int onSpeedZ(MM::PropertyBase* prop, MM::ActionType eact);
	int onRampZ(MM::PropertyBase* prop, MM::ActionType eact);
	int onDirectionZ(MM::PropertyBase* prop, MM::ActionType eact);
	int onBacklashZ(MM::PropertyBase* prop, MM::ActionType eact);
  int onTriggerXYPostDelay(MM::PropertyBase* prop, MM::ActionType eact);
  int onTriggerXYPreDelay(MM::PropertyBase* prop, MM::ActionType eact);
  int onTriggerZPostDelay(MM::PropertyBase* prop, MM::ActionType eact);
  int onTriggerZPreDelay(MM::PropertyBase* prop, MM::ActionType eact);
  int onSetPulseDurationZ(MM::PropertyBase* prop, MM::ActionType eact);
  int onSetPulseDurationX(MM::PropertyBase* prop, MM::ActionType eact);
  int onSetPulseDurationY(MM::PropertyBase* prop, MM::ActionType eact);
  int onZStackSetInstance(MM::PropertyBase* prop, MM::ActionType eact);
    int onZTrigger(MM::PropertyBase* prop, MM::ActionType eact);
    int onEnableTrigger(MM::PropertyBase* prop, MM::ActionType eact);
    int onZStackYSteps(MM::PropertyBase* prop, MM::ActionType eact);
    int onZStackXSteps(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZStack(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZStackWithDelay(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZStackSize(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZStackStepSize(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZFgBgMap(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string name_;
	double curSpeedZ_;
	double rampSpeedZ_;
	double zDirection_;
	long int curZSteps_;
	long int limitZSteps_;
	int backlashZ_;
    int zTrigger_;
    int XYtriggerPreDelay_;
    int XYtriggerPostDelay_;
    int ZtriggerPreDelay_;
    int ZtriggerPostDelay_;
    int ZStackSetInstance_;
    int SetPulseDurationX_;
    int SetPulseDurationY_;
    int SetPulseDurationZ_;
	bool restart_;
	int homeCounter_;
	double microStepsInUmZ_;
    int getEnableTrigger_;
	double getMicroStepsInUmZ_();
    std::vector<double> zStack_;
    int zStackYSteps_;
    int zStackXSteps_;
    long zStackSize_;
    double zStackStepSize_;
    std::string zFgBgMap_;

};

class XYZCom : public CGenericBase <XYZCom>
{
public : 
	XYZCom();
	~XYZCom();

	bool Busy();
	void GetName(char* name) const;
	int Initialize();
	int Shutdown();
	static  long int limitX_;
	static  long int limitY_;
	static  long int  limitZ_;

	static long int microStepX_; 
	static long int microStepY_;
	static long int microStepZ_;
    static long int microStepTurret_;
    static long int XYRunCurrent_;
    static long int ZRunCurrent_;
    static long int TurretRunCurrent_;
    static long int XYHoldCurrent_;
    static long int ZHoldCurrent_;
    static long int TurretHoldCurrent_;
    static long int modeX_;
    static long int modeY_;
    static long int modeZ_;
    static long int modeT_;

	//Action Interface 
	int onEnableXYZ(MM::PropertyBase* prop, MM::ActionType eact);

	int onSetLimitX(MM::PropertyBase* prop, MM::ActionType eact);
	int onSetLimitY(MM::PropertyBase* prop, MM::ActionType eact);
	int onSetLimitZ(MM::PropertyBase* prop, MM::ActionType eact);
	int onEnableSetLimitXYZ(MM::PropertyBase* Prop, MM::ActionType eact);

	int onSetMicroStepX(MM::PropertyBase* prop, MM::ActionType eact);
	int onSetMicroStepY(MM::PropertyBase* prop, MM::ActionType eact);
	int onSetMicroStepZ(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetMicroStepTurret(MM::PropertyBase* prop, MM::ActionType eact); 
    int onSetTurretRunCurrent(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetXYRunCurrent(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZRunCurrent(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetXYHoldCurrent(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetZHoldCurrent(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetTurretHoldCurrent(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetModeX(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetModeY(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetModeZ(MM::PropertyBase* prop, MM::ActionType eact);
    int onSetModeT(MM::PropertyBase* prop, MM::ActionType eact);
    int onEnableSetMicroStepXYZT(MM::PropertyBase* Prop, MM::ActionType eact);
    int onEnableCurrentSettingXY(MM::PropertyBase* Prop, MM::ActionType eact);
    int onEnableCurrentSettingZ(MM::PropertyBase* Prop, MM::ActionType eact);
    int onEnableCurrentSettingTurret(MM::PropertyBase* Prop, MM::ActionType eact);

	int onSetFLDFilter(MM::PropertyBase* prop, MM::ActionType eact);
    int onGetInfo(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string name_;
	int getEnableXYZ_;
	int getEnableSetLimitXYZ_;
    int getEnableSetMicroStepXYZT_;
    int getEnableCurrentSettingXY_;
    int getEnableCurrentSettingZ_;
    int getEnableCurrentSettingTurret_;
	int getFLDFilter_;
    std::string version_;
    std::string date_;
    int infoCheck_;

};

class Lamp : public CShutterBase <Lamp>
{
public :
	Lamp();
	~Lamp();

	bool Busy();
	void GetName(char* name) const;
	int  Initialize();
	int Shutdown();

	//Shutter API
	int SetOpen(bool state);
	int GetOpen(bool& state);
	int Fire(double deltaT);
	
	//Action Interface yet to be implemented 
	int onLedIllumination(MM::PropertyBase* prop, MM::ActionType eact);
  int onOneXLedIllumination(MM::PropertyBase* prop, MM::ActionType eact);
  int onSetLedPower(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string name_;
	bool open_;
	int getLedIllumination_;
    int getOneXLedIllumination_;
    int ledIlluminationState_;
    int oneXLedIlluminationState_;
    int getLedPower_;
};

class ObjLens : public CStateDeviceBase <ObjLens>
{
public:
	ObjLens();
	~ObjLens();

	bool Busy();
	void GetName(char* name) const;
	int Initialize();
	int Shutdown();
	int Home();
	//State Device API
	int SetPosition(long pos);
	int SetPosition(const char* label);
	int GetPosition(long& pos);
	int GetPosition(const char* label);
	int GetPositionLabel(long pos, const char* label);
	int GetLabelPosition(const char* label, long& pos);
	int SetPositionLabel(long pos, const char* label);
	unsigned long GetNumberOfPositions() const;
	int SetGateOpen(bool open);
	int GetGateOpen(bool& open);

	// Acttion property 
	int onSetObj(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string name_;
	int pos_;
	std::string posChar_;
	long objSteps_;
	int objDir_;
	bool restart_;
	int objSpeed_;

	int SetObjMotorON_(bool state);
	int GetXYZSteps_(long& xSteps, long& ySteps, long& zSteps);
	int GetObjSteps_(long& objSteps);
	int SetObjSpeed_(int speed);
	int SetObjMicroStepping_(bool status);
	int SetHome_();
 };

class JoyStick : public CGenericBase <JoyStick>
{
public:
	JoyStick();
	~JoyStick();

	bool Busy();
	void GetName(char* name) const;
	int Initialize();
	int Shutdown();

	static bool JoyStickEnabled_;

	// Action handlers 
	int onSpeedJS(MM::PropertyBase* prop, MM::ActionType eact);
	int onEnableJS(MM::PropertyBase* prop, MM::ActionType eact);

private:
	bool initialized_;
	std::string name_;
	long speedJS_;
	int getEnableJS_;
};

#endif // !_VulanScope_H_
