///////////////////////////////////////////////////////////////////////////////////////////////////
// FILE:          VulanScopeHub.h
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   VulanScopeHub module. Required for operation of VulanScope device.
//                                                                                     
// AUTHOR:        GireeshWaran, 6-November-2016
//                Based on LeicaDSMTC adapter by G. Esteban Fernandez
//				  and LabEnDMI by Manish Shiralakr
//
#ifndef _VulanScopeHub_H_
#define _VulanScopeHub_H_
#include "../../MMDevice/MMDevice.h"


class VulanScopeHub
{
public:
	VulanScopeHub();
	~VulanScopeHub();

	void SetPort(const char* port);
	int Initialize(MM::Device& device, MM::Core& core);
	int DeInitialize();
	bool Initialized();
	std::string Version();
	std::string Microscope();

	//  Sends serial command in the form of string and gets a ack also the form of string
	int SerialCommand(MM::Device& device, MM::Core& core, std::string command, std::string &response);
	//  Sends serial command in the form of string, no Ack for high speed proc
	int SetSerialCommand(MM::Device& device, MM::Core& core, std::string command, std::string &response);
	//caluclates crc and appends it before passing to the serial command
	std::string CrcCalculator(std::string input);

private :
	void _clearRcvBuff();
	void _clearAllRcvBuff(MM::Device& device, MM::Core& core);

	// get commands to be added if needed. 
	std::string port_;
	std::string version_;
	std::string microscope_;
	bool initialized_;

	static const int RCV_BUF_LENGTH = 1024;
	char rcvbuf_[RCV_BUF_LENGTH];

};

#endif // _VulanScopeHub_H_

